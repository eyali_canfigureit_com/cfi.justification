﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using cfi.justification.lib.Scenario;
using cfi.justification.lib.DomnainMediation;
using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Metadata;
using cfi.justification.lib.Suggestion;
using cfi.geometry.lib.Scene;

namespace cfi.Justification.libUnitTest
{
    [TestClass]
    public class JstScenarioRepositoryTest
    {
        static GeoDomainMediatorLoader dummy = GeoDomainMediatorLoader.Loader;

        [TestMethod]
        public void TestLoadScenario()
        {
            int ScenarioCount = JstScenarioRepository.TheInstance.AllScenarioInstantiations().Length;
            JstScenarioRepository.TheInstance.CreateScenarioInstantiation("ThreePredScen");
            JstScenario[] allScenarios = JstScenarioRepository.TheInstance.AllScenarioInstantiations();
            Assert.AreEqual(ScenarioCount + 1, allScenarios.Length);
        }

        [TestMethod]
        public void TestLoadScenario25new()
        {
            int ScenarioCount = JstScenarioRepository.TheInstance.AllScenarioInstantiations().Length;
            JstScenarioRepository.TheInstance.CreateScenarioInstantiation("25new");
            JstScenario[] allScenarios = JstScenarioRepository.TheInstance.AllScenarioInstantiations();
            Assert.AreEqual(ScenarioCount + 1, allScenarios.Length);
        }

        [TestMethod]
        public void TestSuggestions()
        {
            int ScenarioCount = JstScenarioRepository.TheInstance.AllScenarioInstantiations().Length;
            var scen = JstScenarioRepository.TheInstance.CreateScenarioInstantiation("Diagonal bisectors 2");
            JstScenario[] allScenarios = JstScenarioRepository.TheInstance.AllScenarioInstantiations();
            Assert.AreEqual(ScenarioCount + 1, allScenarios.Length);
            var suggestionEngine = new JstSuggestionEngine(scen);
            var suggestions = suggestionEngine.Suggest(null, null);
            Assert.IsTrue(suggestions.Count > 0);
        }

        [TestMethod]
        public void TestAddJust()
        {
            PrivateTestAddJust();
        }

        private JstPredicateInstantiation PrivateTestAddJust()
        { 
            var scen = JstScenarioRepository.TheInstance.CreateScenarioInstantiation("ThreePredScen");
            var cntJst = scen.JustifyPredicateList.Count;
            var predInst = JstScenarioRepository.TheInstance.CreatePredicateInstantiation(scen.ID);
            predInst.SetReferencePredicateKey("CONGRUENT_SEGMENTS");
            var stat = scen.AddToJustify(predInst);
            Assert.IsFalse(stat.IsOK);
            Assert.IsTrue(cntJst == scen.JustifyPredicateList.Count);
            var symbolDictionary = new JstSymbolDictionary(null);
            symbolDictionary.AddTranslation("seg1", "", "AB");
            symbolDictionary.AddTranslation("seg2", "", "CD");
            predInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            stat = scen.AddToJustify(predInst);
            Assert.IsTrue(stat.IsOK);
            Assert.IsTrue(cntJst + 1 == scen.JustifyPredicateList.Count);
            return predInst;
        }

        [TestMethod]
        public void TestIsForOutputPredicate()
        {
            JstScenarioRepository.TEMP_BYPASS_CMS = false;

            JstPredicateInstantiation predInst = PrivateTestAddJust();
            var scen = predInst.Scenario;

            var derList = scen.DerivationListByOutputCategory("", predInst.ReferencePredicate);
            Assert.IsFalse(JstMetadataRepository.TheInstance.AllDerivations().Length == derList.Count);

            Assert.IsTrue(derList.Exists(der => der.Key == "CONGRUENT_SEGMENTS_FROM_EQUAL_MEASURES"));
        }
        [TestMethod]
        public void TestIsForUsePredicate()
        {
            JstScenarioRepository.TEMP_BYPASS_CMS = false;

            JstPredicateInstantiation predInst = PrivateTestAddJust();
            var scen = predInst.Scenario;

            var derList = scen.DerivationListByRelatedCategory("", predInst.ReferencePredicate);
            Assert.IsFalse(JstMetadataRepository.TheInstance.AllDerivations().Length == derList.Count);

            Assert.IsFalse(derList.Exists(der => der.Key == "CONGRUENT_SEGMENTS_FROM_EQUAL_MEASURES"));
        }

        [TestMethod]
        public void TestProve()
        {
            JstScenarioRepository.TEMP_BYPASS_CMS = false;

            JstPredicateInstantiation predInst = PrivateTestAddJust();
            var scen = predInst.Scenario;

            var cntJst = scen.JustifyPredicateList.Count;
            var cntknow = scen.KnowList.Count;

            var derInst = JstScenarioRepository.TheInstance.CreateDerivationInstantiation("CONGRUENT_SEGMENTS_FROM_EQUAL_MEASURES",
                "", predInst.ID);
            //            var stat = scen.MoveToKnow(predInst);
            //            Assert.IsFalse(stat.IsOK);
            //            Assert.IsTrue(cntJst == scen.JustifyPredicateList.Count);

            var symbolDictionary = new JstSymbolDictionary(null);
            symbolDictionary.AddTranslation("seg1", "", "AB");
            symbolDictionary.AddTranslation("seg2", "", "C");
            derInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            var stat = scen.MoveToKnow(predInst);
            Assert.IsFalse(stat.IsOK); // C is not a segment 
            Assert.IsTrue(cntJst == scen.JustifyPredicateList.Count);

            symbolDictionary.UpdateTranslation("seg2", "EF");
            derInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            stat = scen.MoveToKnow(predInst);
            Assert.IsFalse(stat.IsOK);

            symbolDictionary.UpdateTranslation("seg2", "DC"); // should be OK
            derInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            stat = derInst.Validate(false);
            Assert.IsTrue(stat.IsOK);

            symbolDictionary.UpdateTranslation("seg2", "CD"); // should be OK
            derInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            stat = scen.MoveToKnow(predInst);
            Assert.IsTrue(stat.IsOK);

            Assert.IsTrue(cntJst - 1 == scen.JustifyPredicateList.Count);
            Assert.IsTrue(cntknow + 1 == scen.KnowList.Count);


        }
        [TestMethod]
        public void TestProveFromTIK()
        {
            JstScenarioRepository.TEMP_BYPASS_CMS = false;

            var scen = JstScenarioRepository.TheInstance.CreateScenarioInstantiation("ThreePredScen");
            var cntJst = scen.JustifyPredicateList.Count;
            var cntknow = scen.KnowList.Count;
            var knowInst = JstScenarioRepository.TheInstance.PredicateInstantiationByID(scen.KnowList[0].ID);

            var derList = scen.DerivationListByRelatedCategory("", knowInst.ReferencePredicate);
            var derKey = derList[0].Key;

            var derInst = JstScenarioRepository.TheInstance.CreateDerivationInstantiation(derKey,
                knowInst.ID, "");

            var symbolDictionary = new JstSymbolDictionary(null);
            var target = JstScenarioRepository.TheInstance.PredicateInstantiationByID(derInst.ID);
            var stat = scen.MoveToKnow(target);
            Assert.IsTrue(stat.IsOK); //
        }

        [TestMethod]
        public void TestAngles()
        {
            var scen = JstScenarioRepository.TheInstance.CreateScenarioInstantiation("ThreePredScen");
            var predInst = JstScenarioRepository.TheInstance.CreatePredicateInstantiation(scen.ID);
            predInst.SetReferencePredicateKey("CONGRUENT_ANGLES");
            var stat = scen.AddToJustify(predInst);
            Assert.IsFalse(stat.IsOK);
            var symbolDictionary = new JstSymbolDictionary(null);
            symbolDictionary.AddTranslation("ang1", "", "AEC");
            symbolDictionary.AddTranslation("ang2", "", "ABD");
            predInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            stat = scen.AddToJustify(predInst);
            Assert.IsFalse(stat.IsOK);
            symbolDictionary.UpdateTranslation("ang2", "CDE");
            predInst.SymbolDictionary.ImportDictionary(symbolDictionary);
            stat = scen.AddToJustify(predInst);
            Assert.IsTrue(stat.IsOK);
        }

        [TestMethod]
        public void TestLoadGeom()
        {
            var scen = JstScenarioRepository.TheInstance.CreateScenarioInstantiation("ThreePredScen");
            GeoScene scene = GeoSceneRepository.TheInstance.SceneByID(scen.ID);
            Assert.IsTrue(scene.Extension.SceneDisplay.AtomicSectionList.Count > 0);
            Assert.IsTrue(scene.Extension.SceneInteraction.QuadlateralByAtomicSectionDictionary["AE"].Count > 0);
            Assert.IsTrue(scene.Extension.SceneInteraction.QuadlateralByAtomicSectionDictionary["BE"].Count == 0);
        }
    }
}
