﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cfi.justification.lib.Metadata;
using cfi.justification.lib;
using System.Collections.Generic;
using System.Xml;
using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Scenario;
using cfi.justification.lib.Validation;
using cfi.justification.lib.DomnainMediation;

namespace cfi.Justification.libUnitTest
{
    [TestClass]
    public class JstMetadataRepositoryTest
    {
        static GeoDomainMediatorLoader dummy = GeoDomainMediatorLoader.Loader;

        [TestMethod]
        public void TestCMSConnection()
        {
            JstPredicate[] allPredicates = JstMetadataRepository.TheInstance.AllPredicates();
            Assert.AreEqual(true, allPredicates.Length > 0);
        }

        [TestMethod]
        public void TestInstantiatePredicate()
        {
            JstScenario aScenario = JstScenarioRepository.TheInstance.CreateScenarioInstantiation("ThreePredScen");

            JstPredicateInstantiation inst = new JstPredicateInstantiation(aScenario);
            inst.SetReferencePredicateKey("CONGRUENT_SEGMENTS");
            Assert.AreEqual("", inst.SymbolDictionary.TranslateKey("seg1"));
            inst.SymbolDictionary.UpdateTranslation("seg1", "DB");
            Assert.AreEqual("DB", inst.SymbolDictionary.TranslateKey("seg1"));
            JstValidationResult res = inst.Validate(false);
            Assert.AreEqual(res.MessageList.Count, 2);
            inst.SymbolDictionary.UpdateTranslation("seg1", "BA");
            res = inst.Validate(false);
            Assert.AreEqual(res.MessageList.Count, 1);
            
        }

    }
}
