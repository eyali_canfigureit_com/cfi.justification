﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    class JstDisplayDefinitionPrefix : JstDisplayDefinitionBase
    {
        public override String DDType
    {
        get
        {
            return "DDIcon";
        }
    }

    public JstDisplayDefinitionPrefix(String pref)
    {
            Prefix = pref;
    }

    public String Prefix { get; set; }

    public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
    {
        return new JstDisplayDefinitionPrefix(Prefix);
    }

}}
