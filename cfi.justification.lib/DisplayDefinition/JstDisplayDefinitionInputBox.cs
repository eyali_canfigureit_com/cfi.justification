﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    class JstDisplayDefinitionInputBox : JstDisplayDefinitionBase 
    {
        public override String DDType
        {
            get
            {
                return "DDInputBox";
            }
        }

        public JstDisplayDefinitionInputBox(String symbolKey, String symbolType, bool measurement)
        {
            SymbolKey = symbolKey;
            SymbolType = symbolType;
            Measurement = measurement;
        }

        public String SymbolKey { get; set; }
        public String SymbolType { get; set; }
        public bool Measurement { get; set; }

        public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
        {
            return new JstDisplayDefinitionSymbolValue(dict.TranslateKey(SymbolKey),  SymbolType,  Measurement);
        }

    }
}
