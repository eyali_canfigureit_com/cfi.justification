﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    public class JstDisplayDefinitionParser
    {
        static string[] lineSeparators = new string[] { "*line-break*", "#NL" };
        static string[] tokenSeparators = new string[] { "*", "@" };
        const string JST_POINT = "JST_POINT"; 
		const string JST_SEGMENT = "JST_SEGMENT"; // should ask mediator for "JST_SEGMENT" etc
		const string JST_ANGLE = "JST_ANGLE";
		const string JST_TRIANGLE = "JST_TRIANGLE";
		const string JST_QUAD = "JST_QUAD";
		const string JST_CIRCLE = "JST_CIRCLE";
		const string JST_NUMBER = "JST_NUMBER";


        public JstDisplayDefinitionBase FromString(string source)
        {
            if (source == "")
                return null;
            string[] lines = source.Split(lineSeparators,
                                  StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length == 1)
                return LineFromString(lines[0]);
            JstDisplayDefinitionContainer vert = new JstDisplayDefinitionContainer(JstDisplayDefinitionOrientation.Vertical);
            vert.SubElements = new JstDisplayDefinitionBase[lines.Length];
            for (int i = 0; i < lines.Length; i++)
                vert.SubElements[i] = LineFromString(lines[i]);
            return vert;
        }

        private JstDisplayDefinitionBase LineFromString(string strLine)
        {
            JstDisplayDefinitionContainer lineDef = new JstDisplayDefinitionContainer(JstDisplayDefinitionOrientation.Horizontal);
            string[] tokens = strLine.Split(tokenSeparators, StringSplitOptions.RemoveEmptyEntries);
            List<JstDisplayDefinitionBase> lst = new List<JstDisplayDefinitionBase>();
            for (int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i] == "\n")
                    continue;
                string suffix = GetSuffix(tokens[i]);
                if (suffix == "input")
                    lst.Add(DisplayDefinitionInputBox(tokens[i], StripSymbolKey(tokens[++i])));
                else if (suffix == "sign")
                    lst.Add(new JstDisplayDefinitionIcon(tokens[i]));
                else if (GetPrefix(tokens[i]) == "prefix")
                    lst.Add(new JstDisplayDefinitionPrefix(tokens[i]));
                else
                    lst.Add(new JstDisplayDefinitionText(tokens[i]));
            }
            lineDef.SubElements = lst.ToArray<JstDisplayDefinitionBase>();

            return lineDef;
        }

        private string GetSuffix(string token)
        {
            int ind = token.LastIndexOf("-");
            if (ind == -1)
                return token;
            return token.Substring(ind + 1);
        }


        private string GetPrefix(string token)
        {
            int ind = token.IndexOf("-");
            if (ind == -1)
                return token;
            return token.Substring(0, ind);
        }

        private JstDisplayDefinitionInputBox DisplayDefinitionInputBox(string inputType, string symbolKey)
        {
            string jstType = "";
            bool measurement = false;
            switch (inputType)
            {
                case "number-input":
                    jstType = JST_NUMBER;
                    break;
                case "point-input":
                    jstType = JST_POINT;
                    break;
                case "segment-input":
                    jstType = JST_SEGMENT;
                    break;
                case "measure-segment-input":
                    jstType = JST_SEGMENT;
                    measurement = true;
                    break;
                case "angle-input":
                    jstType = JST_ANGLE;
                    break;
                case "measure-angle-input":
                    jstType = JST_ANGLE;
                    measurement = true;
                    break;
                case "circle-input":
                    jstType = JST_CIRCLE;
                    break;
                case "quad-input":
                    jstType = JST_QUAD;
                    break;
                case "triangle-input":
                    jstType = JST_TRIANGLE;
                    break;
            }
            return new JstDisplayDefinitionInputBox(symbolKey, jstType, measurement);
        }


        private string StripSymbolKey(string symKey)
        {
            return symKey;
        }
    }
}
