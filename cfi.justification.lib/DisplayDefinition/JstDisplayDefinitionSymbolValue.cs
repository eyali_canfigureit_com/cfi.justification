﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    public class JstDisplayDefinitionSymbolValue : JstDisplayDefinitionBase
    {
        public override String DDType
        {
            get
            {
                return "DDSymbolValue";
            }
        }

        public JstDisplayDefinitionSymbolValue(String symbolValue, String symbolType, bool measurement)
        {
            SymbolValue = symbolValue;
            SymbolType = symbolType;
            Measurement = measurement;
        }

        public String SymbolValue { get; set; }
        public String SymbolType { get; set; }
        public bool Measurement { get; set; }

        public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
        {
            return new JstDisplayDefinitionSymbolValue(SymbolValue, SymbolType, Measurement);
        }
    }
}
