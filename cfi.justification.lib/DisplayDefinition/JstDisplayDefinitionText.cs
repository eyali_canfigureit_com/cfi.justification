﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    public class JstDisplayDefinitionText : JstDisplayDefinitionBase
    {
        public override String DDType
        {
            get
            {
                return "DDText";
            }
        }

        public JstDisplayDefinitionText(String text)
        {
            Text = text;
        }
        public String Text { get; set; }

        public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
        {
            return new JstDisplayDefinitionText(Text);
        }

    }
}
