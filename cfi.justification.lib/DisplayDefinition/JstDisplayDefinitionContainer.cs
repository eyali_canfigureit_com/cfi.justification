﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    public enum JstDisplayDefinitionOrientation { Horizontal, Vertical };

    public class JstDisplayDefinitionContainer : JstDisplayDefinitionBase
    {
        public override String DDType
        {
            get
            {
                return "DDContainer";
            }
        }

        private JstDisplayDefinitionOrientation mOrientation;
        public JstDisplayDefinitionContainer(JstDisplayDefinitionOrientation orientation)
        {
            Orientation = orientation;
        }

        public JstDisplayDefinitionBase[] SubElements { get; set; }
        public JstDisplayDefinitionOrientation Orientation { get;}

        public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
        {
            JstDisplayDefinitionContainer ret = new JstDisplayDefinitionContainer(Orientation);
            List<JstDisplayDefinitionBase> lst = new List<JstDisplayDefinitionBase>();

            foreach (JstDisplayDefinitionBase sub in SubElements)
                lst.Add(sub.Tranlate(dict));
            ret.SubElements = lst.ToArray();
            return ret;
        }

    }
}
