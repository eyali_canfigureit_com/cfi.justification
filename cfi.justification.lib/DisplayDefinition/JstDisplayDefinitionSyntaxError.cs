﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    class JstDisplayDefinitionSyntaxError : JstDisplayDefinitionBase
    {
        public override String DDType
        {
            get
            {
                return "DDSyntaxError";
            }
        }

        public JstDisplayDefinitionSyntaxError(String erronousText)
        {
            ErronousText = erronousText;
        }

        public String ErronousText { get; set; }

        public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
        {
            return new JstDisplayDefinitionSyntaxError(ErronousText);
        }


    }
}
