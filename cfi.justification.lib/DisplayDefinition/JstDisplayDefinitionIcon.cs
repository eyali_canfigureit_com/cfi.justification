﻿using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.DisplayDefinition
{
    public class JstDisplayDefinitionIcon : JstDisplayDefinitionBase
    {
        public override String DDType
        {
            get
            {
                return "DDIcon";
            }
        }

        public JstDisplayDefinitionIcon(String iconKey)
        {
            IconKey = iconKey;
        }

        public String IconKey { get; set; }

        public override JstDisplayDefinitionBase Tranlate(JstSymbolDictionary dict)
        {
            return new JstDisplayDefinitionIcon(IconKey);
        }

    }
}
