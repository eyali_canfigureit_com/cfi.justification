﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Metadata
{
    public class JstOperandDefinition : JstReadWrite
    {
        public String Key { get; set; }
        public String Type { get; set; }

        public void readFromXML(XmlNode _xml)
        {
            this.Key = readStringAttributeFromXML(_xml, "Key");
            this.Type = readStringAttributeFromXML(_xml, "JstType");
        }
    }
}
