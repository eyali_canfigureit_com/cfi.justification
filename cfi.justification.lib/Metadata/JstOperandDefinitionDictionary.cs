﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Metadata
{
    public class JstOperandDefinitionDictionary
    {
        public JstOperandDefinition[] OperandArray { get; private set; }
        public void readFromXML(XmlNode _xml)
        {
            List<JstOperandDefinition> lst = new List<JstOperandDefinition>();
            foreach (XmlNode node in _xml.SelectNodes("Operand"))
            {
                var opDef = new JstOperandDefinition();
                opDef.readFromXML(node);
                lst.Add(opDef);
            }
            OperandArray = lst.ToArray<JstOperandDefinition>();
        }

        public string TypeForKey(string key)
        {
            foreach (JstOperandDefinition op in OperandArray)
            {
                if (op.Key == key)
                    return op.Type;
            }
            return "";
        }
    }
}
