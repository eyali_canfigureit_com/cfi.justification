﻿using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Scenario;
using cfi.justification.lib.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Metadata
{
    public class JstPredicateReference : JstReadWrite
    {
        public JstDerivation Derivation { get; private set; }
        public List<JstOperandReference> OperandRefArray { get; private set; }
        public String RefKey { get; set; }

        public JstPredicate ReferencePredicate
        {
            get
            {
                return JstMetadataRepository.TheInstance.PredicateByID(this.RefKey);
            }
        }

        public JstPredicateReference(JstDerivation derivation)
        {
            Derivation = derivation;
        }
        public void ReadFromXML(XmlNode xml)
        {
            RefKey = readStringAttributeFromXML(xml, "RefKey");
            OperandRefArray = new List<JstOperandReference>();
            foreach (XmlNode operandSubNode in xml.SelectNodes("OperandRef"))
            {
                var operandReference = new JstOperandReference();
                operandReference.ReadFromXML(operandSubNode);
                OperandRefArray.Add(operandReference);
            }
        }

        public JstValidationResult ValidateWithInput(JstSymbolDictionary inputSymDict, JstScenario owningScenario)
        {
            if (ReferencePredicate == null)
                return new JstValidationResult();
            var symDict = TranslateDictionary(inputSymDict);
            var opArray  = new List<String>();
            foreach (JstOperandReference operandReference in OperandRefArray)
            {
                opArray.Add(operandReference.OperandKey);
            }
            return ReferencePredicate.ValidateWithInput(symDict, opArray, owningScenario);

        }

        public JstPredicateInstantiation SatisfyingPredicate(JstSymbolDictionary inputSymDict, JstScenario owningScenario)
        {
            if (ReferencePredicate == null)
                return null;
            var symDict = TranslateDictionary(inputSymDict);
            var opArray = new List<String>();
            foreach (JstOperandReference operandReference in OperandRefArray)
            {
                opArray.Add(operandReference.OperandKey);
            }
            return ReferencePredicate.SatisfyingPredicate(symDict, opArray, owningScenario);
        }

        public JstSymbolDictionary TranslateDictionary(JstSymbolDictionary inputDictionary)
        {
            var symDict = new JstSymbolDictionary(null);
            foreach (JstOperandReference operandReference in OperandRefArray)
            {
                symDict.AddTranslation(operandReference.RefKey, "", inputDictionary.TranslateKey(operandReference.OperandKey));
            }
            return symDict;
        }


        public JstSymbolDictionary ReverseTranslateDictionary(JstSymbolDictionary input)
        {
            var reverseDictionary = new JstSymbolDictionary(null);
            foreach (var operandReference in OperandRefArray)
            {
                reverseDictionary.AddTranslation(operandReference.OperandKey, "", input.TranslateKey(operandReference.RefKey));
            }
            return reverseDictionary;
        }

    }

    public class JstOperandReference : JstReadWrite
    {
        public String RefKey { get; set; }
        public String OperandKey { get; set; }

        public void ReadFromXML(XmlNode _xml)
        {
            this.RefKey = readStringAttributeFromXML(_xml, "RefKey");
            this.OperandKey = readStringAttributeFromXML(_xml, "OperandKey");
	    }	
	
    }

}
