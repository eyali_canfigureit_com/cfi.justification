﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using cfi.justification.lib.DisplayDefinition;
using cfi.justification.lib.Validation;
using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Scenario;

namespace cfi.justification.lib.Metadata
{
    public class JstPredicate : JstReadWrite
    {
        public JstOperandDefinitionDictionary OperandDictionary { get; private set; }
        public String Key { get; private set; }
        public String Name { get; private set; }
        public JstDisplayDefinitionBase DisplayDescription { get; private set; }
        public JstDisplayDefinitionBase SymbolicErrorDescription { get; private set; }
        public String SymbolicExpression { get; private set; }
        public Boolean IsSymetric { get; private set; }
        public Boolean RequiresJustification { get; private set; }
        public int IconID { get; private set; }
        public String GlossaryID { get; private set; }

        public JstPredicate()
        {
            OperandDictionary = new JstOperandDefinitionDictionary();
        }

        public void readFromXML(XmlNode _xml)
        {
            JstDisplayDefinitionParser parser = new JstDisplayDefinitionParser();

            this.Key = readStringAttributeFromXML(_xml, "Key");
            this.Name = readStringAttributeFromXML(_xml, "PredName");
            String displayDescriptionString = readStringAttributeFromXML(_xml, "SymbolicDescription");
            this.DisplayDescription = parser.FromString(displayDescriptionString);
            this.SymbolicErrorDescription = parser.FromString(readStringAttributeFromXML(_xml, "SymbolicErrorDescription"));
            this.SymbolicExpression = readXMLAttributeFromXML(_xml, "SymbolicExpression");
            this.IsSymetric = readBooleanAttributeFromXML(_xml, "IsSymetric");
            this.RequiresJustification = readBooleanAttributeFromXML(_xml, "RequiresJustification");
            this.IconID = readIntAttributeFromXML(_xml, "IconID");
            this.GlossaryID = readStringAttributeFromXML(_xml, "GlossaryID");
            
            if (SymbolicErrorDescription == null)
            {
                String errorDescriptionString = RequiresJustification ?
                        "$You did not show that$ " + displayDescriptionString:
                        "$Metadata warning: Missing error message for predicate " + Key + "$";
                this.SymbolicErrorDescription = parser.FromString(errorDescriptionString);
            }

            OperandDictionary.readFromXML(_xml);
        }

        public JstValidationResult ValidateWithInput(JstSymbolDictionary inputSymDict, List<String> opArray, JstScenario owningScenario)
        {
            var inst = new JstPredicateInstantiation(owningScenario);
            return validateWithInputImplementation(inputSymDict, opArray, owningScenario, inst);
        }

        public JstPredicateInstantiation SatisfyingPredicate(JstSymbolDictionary inputSymDict, List<String> opArray, JstScenario owningScenario)
        {
            var inst = new JstPredicateInstantiation(owningScenario);
            if (validateWithInputImplementation(inputSymDict, opArray, owningScenario, inst).IsOK)
                return inst;
            return null;
        }

        private JstValidationResult validateWithInputImplementation(JstSymbolDictionary inputSymDict, List<String> opArray, JstScenario owningScenario, JstPredicateInstantiation inst) 
		{
            inst.RefKey = Key;
			inst.initializeFromRef(this.OperandDictionary.OperandArray);
			inst.SymbolDictionary.ImportDictionary(inputSymDict);
			var ret = owningScenario.ValidatePredicateInstantiation(inst);
			return ret;
		}
    }
}
