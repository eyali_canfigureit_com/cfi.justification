﻿using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Scenario;
using cfi.justification.lib.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Metadata
{
    public abstract class JstRequirement : JstReadWrite
    {
        protected int IndexInList;
        public JstRequirement Parent { get; protected set; }

        static public JstRequirement CreateFromXML(XmlNode xml, JstDerivation derivation) 
	    {
            JstRequirement req;
            String nodeName = xml.Name;
		    switch (nodeName)
		    {
			    case "PredicateRef":
				    req = new JstPredicateRequirement(derivation);
				    break;
			    case "Separator":
				    req = new JstSeparatorRequirement();
				    break;
			    case "And":
				    req = new JstAndRequirement(derivation);
				    break;
			    case "Or":
				    req = new JstOrRequirement(derivation);
				    break;
			    default:
//				    Logger.error("Wrong requirement type - " + nodeName);
				    return null;
		    }

            req.ReadFromXML(xml);
		    return req;
	    }

	    virtual public int Renumber(JstRequirement parent, int ind) 
	    {
		    IndexInList = ind;
		    Parent = parent;
		    return ind+1;
	    }

        virtual public JstRequirement RequirementAt(int ind)
	    {
		    return IndexInList == ind? this : null;
        }

        abstract public void ReadFromXML(XmlNode xml);
        abstract public JstValidationResult ValidateWithInput(JstSymbolDictionary symDict, JstScenario owningScenario);
        abstract public List<JstPredicateInstantiation> SatisfiedArray(JstSymbolDictionary symDict, JstScenario owningScenario);
        abstract public JstPredicateReference ForUsePredicate(JstPredicate pred);
        abstract public bool RequiresJustificationDependent();
    }

    class JstSeparatorRequirement : JstRequirement
    {

        override public void ReadFromXML(XmlNode xml) { }
        override public JstValidationResult ValidateWithInput(JstSymbolDictionary symDict, JstScenario owningScenario)
        { 
    		return new JstValidationResult();
	    }
	    override public List<JstPredicateInstantiation> SatisfiedArray(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
		    return new List<JstPredicateInstantiation>();
	    }
	    override public  JstPredicateReference ForUsePredicate(JstPredicate pred)
        {
		    return null;
	    }
        override public bool RequiresJustificationDependent()
        {
		    return false;
	    }
    }

    class JstPredicateRequirement : JstRequirement
    {

        JstDerivation Derivation;
        public JstPredicateReference PredicateReference { get; private set; }
	
	    public JstPredicateRequirement(JstDerivation derivation)
        {
            Derivation = derivation;
            PredicateReference = new JstPredicateReference(derivation);
        }

        override public void ReadFromXML(XmlNode xml)
        {
            PredicateReference.ReadFromXML(xml);
        }
        override public JstValidationResult ValidateWithInput(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
            return PredicateReference.ValidateWithInput(symDict, owningScenario);
        }
        override public List<JstPredicateInstantiation> SatisfiedArray(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
            var ret = new List<JstPredicateInstantiation>();
            JstPredicateInstantiation predInst = PredicateReference.SatisfyingPredicate(symDict, owningScenario);
            if (predInst != null)
                ret.Add(predInst);
            return ret;
        }
        override public JstPredicateReference ForUsePredicate(JstPredicate pred)
        {
            return PredicateReference.RefKey == pred.Key ? PredicateReference : null;
        }
        override public bool RequiresJustificationDependent()
        {
            return PredicateReference.ReferencePredicate != null && PredicateReference.ReferencePredicate.RequiresJustification;
        }
    }

    abstract class JstRequirementGroup : JstRequirement
    {

        protected List<JstRequirement> SubArray;
        JstDerivation Derivation;

        public JstRequirementGroup(JstDerivation derivation)
        {
            Derivation = derivation;
            SubArray = new List<JstRequirement>();
        }

        override public int Renumber(JstRequirement parent, int ind)
        {
		    var ret  = base.Renumber(parent, ind);
            foreach (var sub in SubArray)
            {
                ret = sub.Renumber(this, ret);
            }
            return ret;
        }

        override public JstRequirement RequirementAt(int ind)
        {
            if (IndexInList == ind) 
			    return this;
            foreach (var sub in SubArray)
            {
                var ret  = sub.RequirementAt(ind);
                if (ret != null)
                    return ret;
            }
		    return null;
        }

        override public void ReadFromXML(XmlNode xml)
        {
            foreach (XmlNode reqSubNode in xml.ChildNodes)
            {
                SubArray.Add(JstRequirement.CreateFromXML(reqSubNode, Derivation));
            }
        }

        override public JstPredicateReference ForUsePredicate(JstPredicate pred)
	    {
            foreach (var sub in SubArray)
            {
                var ret = sub.ForUsePredicate(pred);
                if (ret != null)
                    return ret;
            }
		    return null;
        }
    }


    class JstAndRequirement : JstRequirementGroup
    {
        public JstAndRequirement(JstDerivation derivation) : base(derivation)
        {
        }

        override public JstValidationResult ValidateWithInput(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
            var ret = new JstValidationResult();
            foreach(var sub in SubArray)
            {
                if ((sub as JstSeparatorRequirement) != null)
                {
                    if (!ret.IsOK)
                        return ret;
                }
                else
                {
                    ret.AppendMessages(sub.ValidateWithInput(symDict, owningScenario));
                    if (!ret.IsOK)
                        return ret;
                }
            }
            return ret;
        }

        override public List<JstPredicateInstantiation> SatisfiedArray(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
            var ret = new List<JstPredicateInstantiation>();
            foreach (var sub in SubArray)
            {
                var subArr = sub.SatisfiedArray(symDict, owningScenario);
                ret.AddRange(subArr);
            }
            return ret;
        }

        override public bool RequiresJustificationDependent()
        {
            foreach (var sub in SubArray)
            {
                if (sub.RequiresJustificationDependent())
                   return true;
            }
            return false;
        }

}

class JstOrRequirement : JstRequirementGroup
{
        public JstOrRequirement(JstDerivation derivation) : base(derivation)
        {
        }

        override public JstValidationResult ValidateWithInput(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
            var cnt = SubArray.Count;
            if (cnt == 0)
                return new JstValidationResult();
            for (int i = 1; i < cnt; i++)
		    {
                var sub = SubArray[i];
                if (sub.ValidateWithInput(symDict, owningScenario).IsOK)
                    return new JstValidationResult();
            }
            return SubArray[0].ValidateWithInput(symDict, owningScenario);
        }

        override public List<JstPredicateInstantiation> SatisfiedArray(JstSymbolDictionary symDict, JstScenario owningScenario)
        {
            var cnt = SubArray.Count;
            for (int i = 0; i < cnt; i++)
		    {
                var sub = SubArray[i];
                var subArr = sub.SatisfiedArray(symDict, owningScenario);
                if (subArr.Count > 0)
                    return subArr;
            }
            return new List<JstPredicateInstantiation>();
        }

        override public bool RequiresJustificationDependent()
        {
            foreach (var sub in SubArray)
            {
                if (!sub.RequiresJustificationDependent())
                    return false;
            }
            return true;
        }

    }
}
