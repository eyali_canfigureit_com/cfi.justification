﻿using cfi.justification.lib.DisplayDefinition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Metadata
{
    public class JstDerivation : JstReadWrite
    {
        public String Key { get; private set; }
        public String Name { get; private set; }
        public String RelatedCategory { get; private set; }
        public String OutputCategory { get; private set; }
        public Boolean IsTrivial { get; private set; }
        public String GlossaryID { get; private set; }

        public JstDisplayDefinitionBase SymbolicInputDescription { get; private set; }
        public JstOperandDefinitionDictionary OperandDictionary { get; private set; }
        public JstRequirement Requirement { get; private set; }
        public JstPredicateReference Target { get; private set; }

        public JstDerivation()
        {
            OperandDictionary = new JstOperandDefinitionDictionary();
            Target = new JstPredicateReference(this);
        }

        public void readFromXML(XmlNode _xml)
        {
            JstDisplayDefinitionParser parser = new JstDisplayDefinitionParser();
            this.Key = readStringAttributeFromXML(_xml, "Key");
            this.Name = readStringAttributeFromXML(_xml, "DerName");
            this.RelatedCategory = readStringAttributeFromXML(_xml, "RelatedCategory");
            this.OutputCategory = readStringAttributeFromXML(_xml, "OutputCategory");
            this.IsTrivial = readBooleanAttributeFromXML(_xml, "Trivial");
            this.GlossaryID = readStringAttributeFromXML(_xml, "GlossaryID");

            this.SymbolicInputDescription = parser.FromString(readStringAttributeFromXML(_xml, "SymbolicInput"));
            OperandDictionary.readFromXML(_xml);
            foreach (XmlNode reqNode in _xml.SelectNodes("Requirement"))
            {
                Requirement = JstRequirement.CreateFromXML(reqNode.ChildNodes[0], this);
            }
            foreach (XmlNode targetNode in _xml.SelectNodes("Target"))
            {
                if (targetNode.ChildNodes.Count > 0)
                    Target.ReadFromXML(targetNode.ChildNodes[0]);
            }




            //       mIconID = readIntAttributeFromXML(_xml, "IconID");
            //        mDescription = readStringAttributeFromXML(_xml, "Description");
            //        mShortDescription = readStringAttributeFromXML(_xml, "ShortDescription");


            /*            for each(var targetSubNode : XML in _xml.Target.PredicateRef)
                        {
                            var predicateReference : JstPredicateReference = new JstPredicateReference(this);
                            predicateReference.readFromXML(targetSubNode);
                            mTargetArray.push(predicateReference);
                        }
                        for each(var reqSubNode : XML in _xml.Requirement)
                        {
                            mRequirement = JstDerivationRequirement.createFromXML(reqSubNode.*[0], this);
                        }
                        mRequirement.renumber(null, 0);*/
        }

        public bool IsInternal()
        {
            return IsTrivial && (GlossaryID == "" || GlossaryID == "null");
        }

        public bool IsForUsePredicate(JstPredicate pred)
        {
            return ForUsePredicate(pred) != null;
        }

        public JstPredicateReference ForUsePredicate(JstPredicate pred)
        {
            return Requirement.ForUsePredicate(pred);
        }

        public bool IsForOutputPredicate(JstPredicate pred)
        {
            return (Target.ReferencePredicate == pred);
        }

    }
}
