﻿using cfi.cms.helper.Justification;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Metadata
{
    public class JstMetadataRepository : IJstPredicateRepository
    {
        private JstPredicate[] mAllPredicates;
        private JstDerivation[] mAllDerivations;

        private static JstMetadataRepository theInstance = null;
        public static JstMetadataRepository TheInstance
        {
            get
            {
                if (theInstance == null)
                {
                    theInstance = new JstMetadataRepository();
                    theInstance.LoadDefaultXML(); 
                }
                return theInstance;
            }
        }
        private JstMetadataRepository()
        {
        }

        public void LoadPredicatesFromXML(XmlDocument predXML)
        {
            List<JstPredicate> lst = new List<JstPredicate>();

            foreach (XmlNode node in predXML.DocumentElement.ChildNodes)
            {
                var pred = new JstPredicate();
                pred.readFromXML(node);
                lst.Add(pred);
            }
            mAllPredicates = lst.ToArray<JstPredicate>();
        }

        public JstPredicate[] AllPredicates()
        {
            return mAllPredicates;
        }

        public JstPredicate PredicateByID(string key)
        {
            return AllPredicates().Where(t => t.Key == key).FirstOrDefault();
        }

        public void LoadDerivationsFromXML(XmlDocument predXML)
        {
            List<JstDerivation> lst = new List<JstDerivation>();

            foreach (XmlNode node in predXML.DocumentElement.ChildNodes)
            {
                var der = new JstDerivation();
                der.readFromXML(node);
                lst.Add(der);
            }
            mAllDerivations = lst.ToArray<JstDerivation>();
        }

        public JstDerivation[] AllDerivations()
        {
            return mAllDerivations;
        }

        public JstDerivation DerivationByID(string key)
        {
            return AllDerivations().Where(t => t.Key == key).FirstOrDefault();
        }

        private void LoadDefaultXML()
        {
            string token = ConfigurationManager.AppSettings["CMSKey"];
            string xmlStr = "";
            if (Scenario.JstScenarioRepository.TEMP_BYPASS_CMS)
            {
                xmlStr = tempPredXML;
            }
            else
            {
                
                IPredicateHelper predHelper = new CFIPredicateHelper();
                var task = Task.Run(async () =>
                {
                    xmlStr = await predHelper.GetPredicatesXml(token);
                });
                task.Wait();
            }
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlStr);
            LoadPredicatesFromXML(xmlDoc);

            if (Scenario.JstScenarioRepository.TEMP_BYPASS_CMS)
            {
                xmlStr = tempDerXML;
            }
            else
            {
                IDerivationHelper derHelper = new CFIDerivationHelper();
                var task = Task.Run(async () =>
                {
                    xmlStr =  await derHelper.GetDerivationsXml(token);
                });
                task.Wait();
            }
            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlStr);
            LoadDerivationsFromXML(xmlDoc);
        }

        private string getSettingsKey(string key)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            if (ConfigurationManager.AppSettings[key] != null)
            {
                string settingsValue = ConfigurationManager.AppSettings[key];
                logger.Debug(key + " found -> " + settingsValue);
                return settingsValue;
            }
            else
            {
                
                logger.Debug(key + " does not exist.");
                return "";
            }
        }

        private static string tempPredXML
        {
            get
            {
                return
                    @"
<JustificationPredicates>
  <Predicate Key=""CONGRUENT_SEGMENTS"" PredName=""Congruent segments"" SymbolicDescription=""*line-break**segment-input*@seg1@ *congruent-sign* *segment-input*@seg2@"" SymbolicExpression="""" SymbolicErrorDescription="""" IsSymetric=""true"" RequiresJustification=""true"" IconID=""2682"" ImplementationStatus=""N/A"" GlossaryID=""483"">
    <Operand Key=""seg1"" JstType=""JST_SEGMENT""/>
    <Operand Key=""seg2"" JstType=""JST_SEGMENT""/>
  </Predicate>
  <Predicate Key=""EQUAL_SEGMENT_MEASUREMENT"" PredName=""Segments with equal measures"" SymbolicDescription=""*measure-segment-input*@seg1@*equal-sign* *measure-segment-input*@seg2@"" SymbolicExpression=""&amp;lt;ExpressionEquation&amp;gt;  &amp;lt;Left&amp;gt;    &amp;lt;ExpressionSymbol subType=&quot;&quot; symbol=&quot;@seg1@&quot;/&amp;gt;  &amp;lt;/Left&amp;gt;  &amp;lt;Right&amp;gt;    &amp;lt;ExpressionSymbol subType=&quot;&quot; symbol=&quot;@seg2@&quot;/&amp;gt;  &amp;lt;/Right&amp;gt;&amp;lt;/ExpressionEquation&amp;gt;"" SymbolicErrorDescription="""" IsSymetric=""true"" RequiresJustification=""true"" IconID=""3065"" ImplementationStatus=""N/A"" GlossaryID="""">
    <Operand Key=""seg1"" JstType=""JST_SEGMENT""/>
    <Operand Key=""seg2"" JstType=""JST_SEGMENT""/>
  </Predicate>
</JustificationPredicates>                     ";
            }
        }

        private static string tempDerXML
        {
            get
            {
                return
                    @"
<JustificationDerivations>
  <Derivation Key=""CONGRUENT_SEGMENTS_FROM_EQUAL_MEASURES"" DerName=""Congruent segments from equal measures"" Trivial=""true"" SymbolicInput=""*measure-segment-input*@seg1@ *equal-sign* *measure-segment-input*@seg2@#NL&#xA;*arrow-sign*#NL&#xA;*segment-input*@seg1@*congruent-sign**segment-input*@seg2@"" RelatedCategory=""JST_SEGMENT"" OutputCategory=""JST_SEGMENT"" GlossaryID=""87"" IconID=""2651"" Description=""Congruent segments have equal measures."" ShortDescription=""Congruent segments have equal measures"" TargetPrintString=""*line-break**segment-input*D::seg1 *congruent-sign* *segment-input*D::seg2"">
    <Operand Key=""seg1"" JstType=""JST_SEGMENT""/>
    <Operand Key=""seg2"" JstType=""JST_SEGMENT""/>
    <Target>
      <PredicateRef RefKey=""CONGRUENT_SEGMENTS"">
        <OperandRef RefKey=""seg1"" OperandKey=""seg1""/>
        <OperandRef RefKey=""seg2"" OperandKey=""seg2""/>
      </PredicateRef>
    </Target>
    <Requirement>
      <And>
        <PredicateRef RefKey=""EQUAL_SEGMENT_MEASUREMENT"">
          <OperandRef RefKey=""seg1"" OperandKey=""seg1""/>
          <OperandRef RefKey=""seg2"" OperandKey=""seg2""/>
        </PredicateRef>
      </And>
    </Requirement>
    <RequirementsPrintString>
      <RequirementLine PrintString="" And""/>
      <RequirementLine PrintString=""1. *measure-segment-input*D::seg1*equal-sign* *measure-segment-input*D::seg2""/>
    </RequirementsPrintString>
  </Derivation>
</JustificationDerivations>                     ";
            }
        }
    }
}
