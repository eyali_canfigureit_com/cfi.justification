﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib
{
    public class JstReadWrite
    {
        static public String readStringAttributeFromXML(XmlNode _xml, String _Key)
        {
            return _xml.Attributes[_Key] == null? "" : _xml.Attributes[_Key].Value;
        }

        static public Boolean readBooleanAttributeFromXML(XmlNode _xml, String _label)
        {
            String str = readStringAttributeFromXML(_xml, _label);
            return str.ToLower() == "true";
        }

        static public int readIntAttributeFromXML(XmlNode _xml, String _label)
        {
            String str = readStringAttributeFromXML(_xml, _label);
            return 0; // temp
        }

        static public string readXMLAttributeFromXML(XmlNode _xml, String _label)
        {
            String str = readStringAttributeFromXML(_xml, _label);
            return str.ToLower(); // temp
        }

        static public String[] readStringArrayAttributeFromXML(XmlNode _xml, String _Key)
        {
            return new String[0];// _xml.Attributes[_Key].Value;
        }

        static public int[] readIntArrayAttributeFromXML(XmlNode xml, String _label)
		{
            var str =  readStringAttributeFromXML(xml, _label);
			if (str == "")
				return new int[]{ };
            string[] strArray  = str.Split(new char[] {';'},
                      StringSplitOptions.RemoveEmptyEntries);

            var intList = new List<int>();
            foreach (var s in strArray)
			{
				intList.Add(Int32.Parse(s));
			}
			return intList.ToArray();
		}

    }
}
