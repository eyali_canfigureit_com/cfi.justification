﻿using cfi.justification.lib.DisplayDefinition;
using cfi.justification.lib.Instantiation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Scenario
{
    public interface IJstScenarioLine
    {
        String ID { get; }
        Boolean IsPredicate { get; }
        int[] KnowFromIndices { get; }
        String DebugString { get; }
        JstDisplayDefinitionBase SymbolicDescription { get; }
        String Status { get; }
        String HowDoIKnow { get; }
        bool SameAs(IJstScenarioLine other);
        // Internal API
        int OwnIndex { get; set; }
        JstDerivationInstantiation FromDerivation { get; }
        int[] TrivialFromIndices { get; }
    }
}
