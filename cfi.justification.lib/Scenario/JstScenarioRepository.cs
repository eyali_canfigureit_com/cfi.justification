﻿using cfi.cms.helper.Justification;
using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Metadata;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Scenario
{
    public class JstScenarioRepository
    {
        public static Boolean TEMP_BYPASS_CMS = false;
        // SCENARIO
        public JstScenario CreateScenarioInstantiation(string scenNme)
        {
            string xmlStr = TEMP_BYPASS_CMS ? tempXML : mScenarioHelper.GetScenarioXmlByName(scenNme);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlStr);
            JstScenario scen = new JstScenario();
            scen.readFromXML(xmlDoc.DocumentElement, true);
            List<String> solutionNames = SolutionNamesList(scenNme);
            foreach (var solName in solutionNames)
            {
                xmlStr = mScenarioHelper.GetScenarioXmlByName(solName);
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlStr);
                var solution = new JstScenario();
                solution.readFromXML(xmlDoc.DocumentElement, false);
                scen.SolutionList.Add(solution);
            }
            mAllScenarios.Add(scen);
            return scen;
        }

        private List<String> SolutionNamesList(string scenNme)
        {
            var ret = new List<String>();
            if (TEMP_BYPASS_CMS)
                return ret;
            foreach (var rev in mScenarioHelper.RevisionList)
            {
                if (rev.name.Length > scenNme.Length && (rev.name.Substring(0, scenNme.Length + 1) == scenNme + "\\" ||
                     rev.name.Substring(0, scenNme.Length + 1) == scenNme + "/"))
                    ret.Add(rev.name);
            }
            return ret;
        }

        public JstScenario[] AllScenarioInstantiations()
        {
            return mAllScenarios.ToArray();
        }

        public JstScenario ScenarioInstantiationByID(String id)
        {
            return mAllScenarios.Where(t => t.ID == id).FirstOrDefault();
        }

        // SCENARIO LINE
        public List<IJstScenarioLine> AllScenarioLines()
        {
            List<IJstScenarioLine> lst = new List<IJstScenarioLine>();
            foreach (var jstScen in mAllScenarios)
            {
                lst.AddRange(jstScen.GivenList);
                lst.AddRange(jstScen.KnowList);
                lst.AddRange(jstScen.JustifyPredicateList);
            }
            return lst;
        }

        public IJstScenarioLine ScenarioLineByID(string id)
        {
            return AllScenarioLines().Where(t => t.ID == id).FirstOrDefault();
        }

        // PREDICATE INSTANTIATION
        public void RegisterePredicateInstantiation(JstPredicateInstantiation inst)
        {
            mAllPredicateInstatiations.Add(inst);
        }

        public JstPredicateInstantiation CreatePredicateInstantiation(string scenID)
        {
            JstPredicateInstantiation predInst = new JstPredicateInstantiation(ScenarioInstantiationByID(scenID));
            return predInst;
        }

        public void DeletePredicateInstantiationByID(string id)
        {
            mAllPredicateInstatiations.Remove(PredicateInstantiationByID(id));
        }

        public JstPredicateInstantiation PredicateInstantiationByID(string id)
        {
            return mAllPredicateInstatiations.Where(t => t.ID == id).First();
        }
        // DERIVATION INSTANTIATION
        public JstDerivationInstantiation CreateDerivationInstantiation(string derivationKey, string knownPredicateID, string outputPredicateID)
        {
            JstPredicateInstantiation outputInst;
            JstPredicateInstantiation knownInst;
            if (knownPredicateID != "")
            {
                knownInst = PredicateInstantiationByID(knownPredicateID);
                outputInst = CreatePredicateInstantiation(knownInst.Scenario.ID);
            }
            else
            {
                outputInst = PredicateInstantiationByID(outputPredicateID);
                knownInst = null;
            }
            var derInst = new JstDerivationInstantiation(outputInst);
            outputInst.FromDerivation = derInst;
            derInst.SetReferenceDerivationKey(derivationKey, knownInst, outputInst);
            return derInst;
        }

        public void DeleteDerivationInstantiationByID(string derID)
        {
            var predInst = mAllPredicateInstatiations.Where(t => t.ID == derID).First();
            predInst.FromDerivation = null;
        }

        public JstDerivationInstantiation DerivationInstantiationByID(string derID)
        {
            var predInst = mAllPredicateInstatiations.Where(t => t.ID == derID).First();
            return predInst.FromDerivation;
        }

        private CFIScenarioHelper mScenarioHelper;
        private List<JstScenario> mAllScenarios;
        private List<JstPredicateInstantiation> mAllPredicateInstatiations;


        private static JstScenarioRepository theInstance = null;
        public static JstScenarioRepository TheInstance
        {
            get
            {
                if (theInstance == null)
                {
                    theInstance = new JstScenarioRepository();
                }
                return theInstance;
            }
        }



        private JstScenarioRepository()
        {
            var dummy = JstMetadataRepository.TheInstance; // This somehow makes cms work OK
            if (!TEMP_BYPASS_CMS)
            {
                mScenarioHelper = new CFIScenarioHelper();
            }
            mAllScenarios = new List<JstScenario>();
            mAllPredicateInstatiations = new List<JstPredicateInstantiation>();
        }

        private static string tempXML
        {
            get
            {
                return
                    @"
<JustificationScenario ScenarioName=""ThreePredScen"" ExcludeDerivations="""">
  <IKnow ind=""0"" RefKey=""CONGRUENT_SEGMENTS"" HowDoIKnow=""Given"" Status=""STATUS_GIVEN_AND_USED"" KnowFromIndices="""" TrivialFromIndices="""">
    <OperandInstanciation RefKey=""seg1"" OperandValue=""EF""/>
    <OperandInstanciation RefKey=""seg2"" OperandValue=""GH""/>
  </IKnow>
  <IKnow ind=""1"" RefKey=""EQUAL_SEGMENT_MEASUREMENT"" HowDoIKnow=""Given"" Status=""STATUS_GIVEN"" KnowFromIndices="""" TrivialFromIndices="""">
    <OperandInstanciation RefKey=""seg1"" OperandValue=""EF""/>
    <OperandInstanciation RefKey=""seg2"" OperandValue=""GH""/>
  </IKnow>
  <IKnow ind=""2"" RefKey=""EQUAL_SEGMENT_MEASUREMENT"" HowDoIKnow=""Congruent segments have equal measures"" Status=""STATUS_NORMAL"" KnowFromIndices=""0"" TrivialFromIndices="""">
    <OperandInstanciation RefKey=""seg1"" OperandValue=""AB""/>
    <OperandInstanciation RefKey=""seg2"" OperandValue=""CD""/>
    <FromDerivation RefKey=""EQUAL_MEASURES_FROM_CONGRUENT_SEG"" Category="""" ConstantKeys=""seg1;seg2"" FieldKeys=""seg1;seg2;seg1;seg2"">
      <OperandInstanciation RefKey=""seg1"" OperandValue=""AB""/>
      <OperandInstanciation RefKey=""seg2"" OperandValue=""CD""/>
    </FromDerivation>
  </IKnow>
  <Trivial ind=""0"" RefKey=""CONGRUENT_SEGMENTS"" HowDoIKnow=""Trivial"" Status=""STATUS_NORMAL"" KnowFromIndices=""2"" TrivialFromIndices="""">
    <OperandInstanciation RefKey=""seg1"" OperandValue=""AB""/>
    <OperandInstanciation RefKey=""seg2"" OperandValue=""CD""/>
    <FromDerivation RefKey=""CONGRUENT_SEGMENTS_FROM_EQUAL_MEASURES"" Category="""" ConstantKeys=""seg1;seg2"" FieldKeys=""seg1;seg2;seg1;seg2"">
      <OperandInstanciation RefKey=""seg1"" OperandValue=""AB""/>
      <OperandInstanciation RefKey=""seg2"" OperandValue=""CD""/>
    </FromDerivation>
  </Trivial>
  <Domain>
    <model version=""201672110571258"">
      <configuration/>
      <data>
        <entityList>
          <point entityId=""22"" labelOffsetX=""5.176380902050415"" hideLabel=""false"" labelOffsetY=""19.318516525781366"" isInternal=""false"" colorIndex=""0"" x=""15.135135135135137"" y=""54.5945945945946"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""A"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""23"" labelOffsetX=""14.142135623730951"" hideLabel=""false"" labelOffsetY=""14.14213562373095"" isInternal=""false"" colorIndex=""0"" x=""52.972972972972975"" y=""23.24324324324325"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""B"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""35"" labelOffsetX=""5.176380902050415"" hideLabel=""false"" labelOffsetY=""19.318516525781366"" isInternal=""false"" colorIndex=""0"" x=""-40"" y=""36.75675675675676"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""C"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""36"" labelOffsetX=""14.142135623730951"" hideLabel=""false"" labelOffsetY=""14.14213562373095"" isInternal=""false"" colorIndex=""0"" x=""-6.486486486486487"" y=""12.972972972972974"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""D"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""63"" labelOffsetX=""14.142135623730951"" hideLabel=""false"" labelOffsetY=""14.14213562373095"" isInternal=""false"" colorIndex=""0"" x=""42.70270270270271"" y=""12.972972972972974"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""E"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""64"" labelOffsetX=""14.142135623730951"" hideLabel=""false"" labelOffsetY=""14.14213562373095"" isInternal=""false"" colorIndex=""0"" x=""23.783783783783782"" y=""-26.486486486486488"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""F"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""102"" labelOffsetX=""14.142135623730951"" hideLabel=""false"" labelOffsetY=""14.14213562373095"" isInternal=""false"" colorIndex=""0"" x=""-0.5405405405405406"" y=""-31.35135135135135"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""G"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""103"" labelOffsetX=""5.176380902050415"" hideLabel=""false"" labelOffsetY=""19.318516525781366"" isInternal=""false"" colorIndex=""0"" x=""-70.81081081081082"" y=""-7.027027027027027"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""H"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""105"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" isInternal=""true"" colorIndex=""0"" x=""3.243243243243243"" y=""34.054054054054056"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""*A"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""106"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" isInternal=""true"" colorIndex=""0"" x=""-10.810810810810812"" y=""-10.810810810810812"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""*B"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""108"" labelOffsetX=""-5.176380902050417"" hideLabel=""false"" labelOffsetY=""19.318516525781366"" isInternal=""false"" colorIndex=""0"" x=""29.04439504797361"" y=""43.06977923824272"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""I"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <point entityId=""109"" labelOffsetX=""-5.176380902050417"" hideLabel=""false"" labelOffsetY=""19.318516525781366"" isInternal=""false"" colorIndex=""0"" x=""40.873693402038214"" y=""33.268360602017765"" tracing=""false"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""J"" congruencyIndex=""0"" isPinned=""false"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""24"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""A"" pt2=""B"" mode=""3"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""AB"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""37"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""C"" pt2=""D"" mode=""3"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""CD"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""65"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""E"" pt2=""F"" mode=""3"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""EF"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""104"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""G"" pt2=""H"" mode=""3"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""GH"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""110"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""A"" pt2=""I"" mode=""0"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""AI"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""111"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""A"" pt2=""J"" mode=""0"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""AJ"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""112"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""B"" pt2=""I"" mode=""0"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""BI"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""113"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""B"" pt2=""J"" mode=""0"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""BJ"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <section pt2free=""true"" entityId=""114"" labelOffsetX=""10"" hideLabel=""false"" labelOffsetY=""10"" drawingOrder=""0"" parallelIndex=""0"" colorIndex=""0"" pt1=""I"" pt2=""J"" mode=""0"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""IJ"" sectionType=""3"" congruencyIndex=""0"" lineStyle=""0""/>
          <circle radialPoint=""*B"" entityId=""107"" labelOffsetX=""5.176380902050415"" hideLabel=""false"" labelOffsetY=""19.318516525781366"" colorIndex=""0"" hide=""false"" showMeasure=""false"" measurementDisplacement=""0.5"" label=""o1"" centralPoint=""*A"" congruencyIndex=""0"" lineStyle=""0""/>
        </entityList>
        <dependencyList>
          <pointOnLine section=""AB"" dependentPoint=""I"" ratio=""0.6323981308749832"" forceRatio=""false"" ruleId=""0""/>
          <pointOnLine section=""AB"" dependentPoint=""J"" ratio=""0.31976667437470446"" forceRatio=""false"" ruleId=""1""/>
        </dependencyList>
      </data>
      <TextTool/>
      <GeoExpression/>
      <expressionTable visible=""false"" width=""350"" height=""250"" x=""30"" y=""100"">
        <expression printString="""" width=""69""/>
        <expression printString="""" width=""69""/>
        <expression printString="""" width=""69""/>
        <expression printString="""" width=""69""/>
        <expression printString="""" width=""72""/>
      </expressionTable>
    </model>
  </Domain>
</JustificationScenario>
                    ";

            }
        }
    }
}
