﻿using cfi.justification.lib.DomnainMediation;
using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Metadata;
using cfi.justification.lib.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Scenario
{
    public class JstScenario : JstReadWrite
    {
        private const string JST_NONE = "JST_NONE";

        public String ID { get; set; }
        public String Name { get; private set; }
        public String ExcludeDerivations { get; private set; }
        public List<IJstScenarioLine> GivenList { get; private set; }
        public List<IJstScenarioLine> KnowList { get; private set; }
        public List<IJstScenarioLine> TrivialList { get; private set; }
        public List<IJstScenarioLine> JustifyPredicateList { get; private set; }
        public List<JstScenario> SolutionList { get; set; }

        public JstPredicateInstantiation JustifyPredicateByID(String id)
        {
            foreach (var jstLine in JustifyPredicateList)
            {
                var pred = jstLine as JstPredicateInstantiation;
                if (pred == null)
                    continue;
                if (pred.ID == id)
                    return pred;
            }
            return null;
        }

        public JstPredicateInstantiation KnownPredicateByID(String id)
        {
            foreach (var jstLine in KnowList)
            {
                var pred = jstLine as JstPredicateInstantiation;
                if (pred == null)
                    continue;
                if (pred.ID == id)
                    return pred;
            }
            return null;
        }

        private IJstDomainMediator mediator;

        private static int NextScenarioID = 0;
        static GeoDomainMediatorLoader dummy = GeoDomainMediatorLoader.Loader;

        public void readFromXML(XmlNode xml, Boolean readDomain)
        {
            if (!verifyPredicateLoad(xml))
                return;

            this.Name = readStringAttributeFromXML(xml, "ScenarioName");
            this.ID = this.Name.Replace(' ', '_') + "_" + (NextScenarioID++).ToString();
            this.ExcludeDerivations = readStringAttributeFromXML(xml, "ExcludeDerivations");

            this.GivenList = readLinesFromXML(xml, "Given", JstPredicateInstantiation.STATUS_GIVEN, "Given", "G");
            this.KnowList = readLinesFromXML(xml, "IKnow", JstPredicateInstantiation.STATUS_GIVEN, "Given", "K");
            this.TrivialList = readLinesFromXML(xml, "Trivial", JstPredicateInstantiation.STATUS_NORMAL, "Trivial", "T");
            this.JustifyPredicateList = readLinesFromXML(xml, "Justify", JstPredicateInstantiation.STATUS_TARGET, "", "J");
            RenumberLineLists();
            if (readDomain)
            {
                this.mediator = IJstDomainMediatorLoader.MediatorByDomain("model");
                this.mediator.ReadDomainFromXML(xml.SelectNodes("Domain")[0], this.ID);
            }
            this.SolutionList = new List<JstScenario>();
        }

        public void RenumberLineLists()
        {
            RenumberList(GivenList);
            RenumberList(KnowList);
            RenumberList(TrivialList);
            RenumberList(JustifyPredicateList);
        }

        public void RenumberList(List<IJstScenarioLine> list)
        {
            int i = 0;
            foreach (var line in list)
            {
                line.OwnIndex = i++;
            }
        }

        private List<IJstScenarioLine> readLinesFromXML(XmlNode xml, string tag, string status, string howDoIKnow, string idPrefix)
        {
            List<IJstScenarioLine> lst = new List<IJstScenarioLine>();
            int cnt = 0;
            foreach (XmlNode xmlNode in xml.SelectNodes(tag))
            {
                String refKey = readStringAttributeFromXML(xmlNode, "RefKey");

                if (false)//refKey == JstAlgExpressionLine.LINE_TYPE)
                {
                    //                    var expHolder : JstAlgExpressionLine = new JstAlgExpressionLine(null);
                    //                    expHolder.readFromXML(xmlNode);
                    //                    _predArray.push(expHolder);
                }
                else
                {
                    JstPredicateInstantiation predInst = new JstPredicateInstantiation(this);
                    predInst.readFromXML(xmlNode);
                    predInst.ID = this.ID + "-" + idPrefix + (cnt++).ToString();
                    if (predInst.Status == "")
                    {
                        predInst.Status = status;
                        predInst.HowDoIKnow = howDoIKnow;
                    }
                    lst.Add(predInst);
                    //                    if (predInst.status == JstPredicateInstanciation.STATUS_TARGET)
                    //                        mGoalPredInst = predInst;

                }
            }
            return lst;
        }

        private bool verifyPredicateLoad(XmlNode _xml)
        {
            return true;// TEMP
        }

        public void ValidateSymbolDictionary(JstSymbolDictionary symDict, JstValidationResult res, Boolean staged)
        {
            mediator.ValidateSymbolDictionary(symDict, res, staged);
        }

        public JstValidationResult AddToJustify(JstPredicateInstantiation newPred)
        {
            var ret = newPred.Validate(false);
            if (ret.IsOK)
                JustifyPredicateList.Add(newPred);
            RenumberList(JustifyPredicateList);
            return ret;
        }

        public JstValidationResult MoveToKnow(JstPredicateInstantiation pred)
        {
            var ret = pred.FromDerivation.Validate(false);
            if (ret.IsOK)
            {
                pred.FromDerivation.FillOutputPredicate(pred);
                JustifyPredicateList.Remove(pred);
                KnowList.Add(pred);
                RenumberList(JustifyPredicateList);
                RenumberList(KnowList);
            }
            return ret;
        }

        public JstValidationResult ValidatePredicateInstantiation(JstPredicateInstantiation predInst)
        {
            return IsKnownOrTrivial(predInst) ?
                new JstValidationResult() :
                new JstValidationResult(predInst.BuildErrorDescription());
        }


        public bool IsKnownOrTrivial(JstPredicateInstantiation predInst)
        {
            var variationArray = predInst.VariationArray();
			foreach(var knownLine in KnowAndTrivialList())
			{
				var knownPred  = knownLine as JstPredicateInstantiation;
				if (knownPred == null)
					continue;
				if (knownPred.RefKey != predInst.RefKey)
					continue;
				foreach (var candPred in variationArray)
                {
                    if (knownPred.Matches (candPred))
                        return true;
                }
            }
			return false;
		}

        public bool MatchOperand(String val1, String val2, String opType)
        {
            return mediator.MatchOperand(val1, val2, opType);
        }

        private List<IJstScenarioLine> KnowAndTrivialList()
        {
            var ret = new List<IJstScenarioLine>();
            ret.AddRange(KnowList);
            ret.AddRange(TrivialList);
            return ret;
        }

        public List<JstDerivation> DerivationListByRelatedCategory(String cat, JstPredicate pred) 
		{
			var ret = new List<JstDerivation>();
			foreach (var der in JstMetadataRepository.TheInstance.AllDerivations())
            {
                if (der.RelatedCategory == "" || der.RelatedCategory == JST_NONE)
                    continue;
                if (der.IsInternal() || ExcludesDerivation(der))
                    continue;
                if ((cat == "" || cat == der.RelatedCategory) && der.IsForUsePredicate(pred))
                    ret.Add(der);
            }
			return ret;
		}

        public List<JstDerivation> DerivationListByOutputCategory(String cat, JstPredicate pred)
        {
            var ret = new List<JstDerivation>();
            foreach (var der in JstMetadataRepository.TheInstance.AllDerivations())
            {
                if (der.IsInternal() || ExcludesDerivation(der))
                    continue;
                if ((cat == "" || cat == der.OutputCategory) &&  der.IsForOutputPredicate(pred))
                    ret.Add(der);
            }
			return ret;
		}	

        private bool ExcludesDerivation(JstDerivation der)
        {
            return false; // todo
        }

        public String MeasuredElement(JstPredicateInstantiation tik)
		{
			return mediator.MeasuredElement(tik);
		}

}
}
