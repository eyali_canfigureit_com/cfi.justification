﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Validation
{

    public class JstMessageBroker
    {
        public const string JST_MANDATORY = "JST_MANDATORY";
        public const string JST_NO_SEGMENT = "JST_NO_SEGMENT";
        public const string JST_NO_POINT = "JST_NO_POINT";
        public const string JST_NO_CIRCLE = "JST_NO_CIRCLE";
        public const string JST_NO_ANGLE = "JST_NO_ANGLE";
        public const string JST_NO_TRIANGLE = "JST_NO_TRIANGLE";
        public const string JST_NO_QUAD = "JST_NO_QUAD";
        public const string JST_NO_NUMBER = "JST_NO_NUMBER";
        public const string JST_NO_TYPE = "JST_NO_TYPE";

        public static String MessageByCode(String code) // to be replaced by XML
        {
            switch (code)
            {
                case JST_MANDATORY:
                    return "Please fill in the information";
                case JST_NO_SEGMENT:
                    return "@p1@ is not a segment on the canvas";
                case JST_NO_POINT:
                    return "@p1@ is not a point on the canvas";
                case JST_NO_CIRCLE:
                    return "@p1@ is not a circle on the canvas";
                case JST_NO_ANGLE:
                    return "@p1@ is not an angle on the canvas";
                case JST_NO_TRIANGLE:
                    return "@p1@ is not a triangle on the canvas";
                case JST_NO_QUAD:
                    return "@p1@ is not a quadrilateral on the canvas";
                case JST_NO_NUMBER:
                    return "@p1@ is not a Number";
                case JST_NO_TYPE:
                    return "@p1@ is not a recognized type";
            }
            return "Message Code '" + code + "' is not recognized";
        }

        public static String MessageByCodeSingleParameter(String code, String param1)
        {
            return EmbedSingleParameter(MessageByCode(code), param1);
        }

        private static String EmbedSingleParameter(String msg, String param1)
        {
            return msg.Replace("@p1@", param1);
        }
    }
}
