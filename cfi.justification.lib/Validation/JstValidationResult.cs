﻿using cfi.justification.lib.DisplayDefinition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Validation
{
    public class JstValidationResult
    {
        public List<JstValidationMessage> MessageList { get; private set; }

        public Boolean IsOK { get { return MessageList.Count == 0; } }

        public JstValidationResult(JstDisplayDefinitionBase description = null)
        {
            MessageList = new List<JstValidationMessage>();
            if (description != null)
                AddMessage(new JstValidationMessage(description));
        }

        public void AddMessage(JstValidationMessage msg)
        {
            MessageList.Add(msg);
        }

        public void AppendMessages(JstValidationResult msgs)
        {
            foreach (var msg in msgs.MessageList)
                AddMessage(msg);
        }
    }
}
