﻿using cfi.justification.lib.DisplayDefinition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Validation
{
    public class JstValidationMessage
    {
        public JstDisplayDefinitionBase Description { get; private set; }
        public String[] Reference { get; set; }

        public JstValidationMessage(JstDisplayDefinitionBase description, String[] reference)
        {
            Description = description;
            Reference = reference;
        }

        public JstValidationMessage(JstDisplayDefinitionBase description)
        {
            Description = description;
            Reference = new string[0];
        }

        public JstValidationMessage(string msg)
        {
            Description = new JstDisplayDefinitionText(msg);
            Reference = new string[0];
        }
    }
}
