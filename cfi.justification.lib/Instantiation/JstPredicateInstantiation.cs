﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cfi.justification.lib.Scenario;
using System.Xml;
using cfi.justification.lib.Metadata;
using cfi.justification.lib.DisplayDefinition;
using cfi.justification.lib.Validation;

namespace cfi.justification.lib.Instantiation
{
    public class JstPredicateInstantiation : JstInstantiation, IJstScenarioLine
    {
        public static string STATUS_GIVEN = "STATUS_GIVEN";
        public static string STATUS_GIVEN_AND_USED = "STATUS_GIVEN_AND_USED";
        public static string STATUS_NORMAL = "STATUS_NORMAL";
        public static string STATUS_TARGET = "STATUS_TARGET";

        public string ID { get; set; }
        public Boolean IsPredicate { get { return true; } }
        public String DebugString { get { return "TODO"; } }
        public JstDisplayDefinitionBase SymbolicDescription { get { return BuildSymbolicDecsription(); } }
        public String Status { get; set; }
        public String HowDoIKnow { get; set; }
        public int[] KnowFromIndices { get; set; }
        public int[] TrivialFromIndices { get; set; }
        public int OwnIndex { get; set; }

        public JstDerivationInstantiation FromDerivation { get; set; }

        private static int NextPredicateInstantiationID = 0;

        public JstPredicateInstantiation(JstScenario scenario) : base(scenario)
        {
            ID = scenario.ID + "-" + (NextPredicateInstantiationID++).ToString();
            FromDerivation = null;
            KnowFromIndices = new int[] { };
            TrivialFromIndices = new int[] { };
            JstScenarioRepository.TheInstance.RegisterePredicateInstantiation(this);
        }

        override public String GetTypeForOperandKey(String key)
        {
            JstPredicate pred = ReferencePredicate;
            if (pred == null)
            {
                return "";
            }
            return pred.OperandDictionary.TypeForKey(key);
        }

        public void SetReferencePredicateKey(String refKey)
        {
            RefKey = refKey;
            if (ReferencePredicate != null)
                initializeFromRef(ReferencePredicate.OperandDictionary.OperandArray);
        }

        public JstPredicate ReferencePredicate
        {
            get
            {
                return JstMetadataRepository.TheInstance.PredicateByID(this.RefKey);
            }
        }

        public override void readFromXML(XmlNode xml)
        {
            base.readFromXML(xml);
            HowDoIKnow = readStringAttributeFromXML(xml, "HowDoIKnow");
            Status = readStringAttributeFromXML(xml, "Status");
            foreach (XmlNode derNode in xml.SelectNodes("FromDerivation"))
            {
                FromDerivation = new JstDerivationInstantiation(this);
                FromDerivation.readFromXML(derNode);
            }
            KnowFromIndices = readIntArrayAttributeFromXML(xml, "KnowFromIndices");
            TrivialFromIndices = readIntArrayAttributeFromXML(xml, "TrivialFromIndices");
        }

        private JstDisplayDefinitionBase BuildSymbolicDecsription()
        {
            if (ReferencePredicate == null)
                return new JstDisplayDefinitionText(RefKey);
            return this.ReferencePredicate.DisplayDescription.Tranlate(SymbolDictionary);
        }

        public JstValidationResult Validate(Boolean staged)
        {
            JstValidationResult res = new JstValidationResult();
            JstPredicate pred = ReferencePredicate;
            if (pred == null)
            {
                res.AddMessage(new JstValidationMessage("Predicate not found"));
                return res;
            }

            Scenario.ValidateSymbolDictionary(SymbolDictionary, res, staged);
            return res;
        }

        public JstValidationResult AddToJustify()
        {
            return Scenario.AddToJustify(this);
        }

        public JstValidationResult MoveToKnow()
        {
            return Scenario.MoveToKnow(this);
        }

        public JstDisplayDefinitionBase BuildErrorDescription()
        {
            if (ReferencePredicate == null)
                return new JstDisplayDefinitionText(RefKey);

            var ret = ReferencePredicate.SymbolicErrorDescription;
            return ret.Tranlate(SymbolDictionary);
        }

        public List<JstPredicateInstantiation> VariationArray()
        {
            var ret = new List<JstPredicateInstantiation>();
            ret.Add(this);
            //            if (IsSymetric)
            //              ret.Add(reverse...);
            return ret;
        }

        public bool Matches(JstPredicateInstantiation other)
        {
            if (other == null)
                return false;

  			if (RefKey != other.RefKey)
				return false;

            if (ReferencePredicate == null)
                return false;
			
			foreach (var op in ReferencePredicate.OperandDictionary.OperandArray)
            {
                var myVal = SymbolDictionary.TranslateKey(op.Key);
                var otherVal = other.SymbolDictionary.TranslateKey(op.Key);
                if (myVal == otherVal)
                    continue;

                if (!Scenario.MatchOperand(myVal, otherVal, op.Type))
                    return false;
            }
			return true;
        }

        public bool SameAs(IJstScenarioLine other)
        {
            var otherPred = other as JstPredicateInstantiation;
            return Matches(otherPred);
        }

    }
}