﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Instantiation
{
    public class JstSymbolDictionary
    {

        public JstOperandInstanciation[] OperandArray { get; set; }

        private JstInstantiation Owner;
        public JstSymbolDictionary(JstInstantiation owner)
        {
            Owner = owner;
            OperandArray = new JstOperandInstanciation[0];
        }

        public void readFromXML(XmlNode xml)
        {
            List<JstOperandInstanciation> lst = new List<JstOperandInstanciation>();

            foreach (XmlNode xmlNode in xml.SelectNodes("OperandInstanciation"))
            {
                var op = new JstOperandInstanciation();
                op.readFromXML(xmlNode);
                lst.Add(op);
            }
            this.OperandArray = lst.ToArray<JstOperandInstanciation>();
        }

        public String TranslateKey(String key)
        {
            foreach (JstOperandInstanciation op in OperandArray)
            {
                if (op.RefKey == key)
                    return op.OperandValue;
            }
            return "@" + key;
        }

        public void AddTranslation(String key, String type, String value)
        {
            List<JstOperandInstanciation> lst = this.OperandArray.ToList<JstOperandInstanciation>();

            var op = new JstOperandInstanciation();
            op.RefKey = key;
            op.OperandValue = value;
            lst.Add(op);

            this.OperandArray = lst.ToArray<JstOperandInstanciation>();
        }

        public void UpdateTranslation(String key, String value)//, _updateType : String = JstModelEvent.UPDATE_TYPE_MISC)
        {
            if (value == null)
                value = "";

            UndoTempTranslation();
            foreach (JstOperandInstanciation op in OperandArray)
            {
                if (op.RefKey == key)
                {
                    if (op.OperandValue == value)
                        return;
                    op.OperandValue = value;
                    //if (JstModel.theInstance.verbalMode)
                    //    dispatchEvent(new JstModelEvent(JstModelEvent.SYMBOL_UPDATE, false, _updateType));
                }
            }
        }

        public void ImportDictionary(JstSymbolDictionary other)
        {
            foreach (JstOperandInstanciation op in OperandArray)
            {
                op.OperandValue = other.TranslateKey(op.RefKey);
            }
        }


        private void UndoTempTranslation()
        {
        }

        public String GetTypeForKey(String key)
        {
            return Owner.GetTypeForOperandKey(key);
        }
    }
}
