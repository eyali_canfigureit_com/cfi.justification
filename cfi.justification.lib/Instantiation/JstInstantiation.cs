﻿using cfi.justification.lib.Metadata;
using cfi.justification.lib.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Instantiation
{
    abstract public class JstInstantiation : JstReadWrite
    {
        private string _refKey;
        public String RefKey
        {
            get { return _refKey; }
            set
            {
                _refKey = value;
                SymbolDictionary = new JstSymbolDictionary(this);
            }
        }

        public JstSymbolDictionary SymbolDictionary { get; private set; }
        public JstScenario Scenario { get; private set; }

        protected JstInstantiation(JstScenario scenario)
        {
            Scenario = scenario;
            SymbolDictionary = new JstSymbolDictionary(this);
        }

        abstract public String GetTypeForOperandKey(String key);

        public virtual void readFromXML(XmlNode xml)
        {
            this.RefKey = readStringAttributeFromXML(xml, "RefKey");
            this.SymbolDictionary.readFromXML(xml);
        }

        public void initializeFromRef(JstOperandDefinition[] operandDefinitionArray)
		{
            SymbolDictionary = new JstSymbolDictionary(this);
            foreach (JstOperandDefinition operand in operandDefinitionArray)
            {
                SymbolDictionary.AddTranslation(operand.Key, operand.Type, "");
            }
        }

    }
}