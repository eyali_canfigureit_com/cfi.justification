﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Instantiation
{
    public class JstOperandInstanciation : JstReadWrite
    {
        public String RefKey { get; set; }
        public String OperandValue { get; set; }

        public void readFromXML(XmlNode xml)
        {
            this.RefKey = readStringAttributeFromXML(xml, "RefKey");
            this.OperandValue = readStringAttributeFromXML(xml, "OperandValue");
        }
    }
}
