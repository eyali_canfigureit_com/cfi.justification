﻿using cfi.justification.lib.Metadata;
using cfi.justification.lib.Scenario;
using cfi.justification.lib.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.Instantiation
{
    public class JstDerivationInstantiation : JstInstantiation
    {
        public string ID { get; private set; }
        public String Category { get; set; }
        public String[] ConstantKeys { get; set; }
        public String[] FieldKeys { get; set; }
        
        override public String GetTypeForOperandKey(String key)
        {
            JstDerivation der = ReferenceDerivation;
            if (der == null)
            {
                return "";
            }
            return der.OperandDictionary.TypeForKey(key);
        }

        public JstDerivation ReferenceDerivation
        {
            get
            {
                return JstMetadataRepository.TheInstance.DerivationByID(this.RefKey);
            }
        }

        public JstDerivationInstantiation(JstPredicateInstantiation pred) : base(pred.Scenario)
        {
            ID = pred.ID;
        }

        public override void readFromXML(XmlNode xml)
        {
            base.readFromXML(xml);
            Category = readStringAttributeFromXML(xml, "Category");
            ConstantKeys = readStringArrayAttributeFromXML(xml, "ConstantKeys");
            FieldKeys = readStringArrayAttributeFromXML(xml, "FieldKeys");
        }

        public void SetReferenceDerivationKey(String refKey, JstPredicateInstantiation knowPredicate, JstPredicateInstantiation outputPredicate)
        {
            RefKey = refKey;
            if (ReferenceDerivation == null)
                return;
            initializeFromRef(ReferenceDerivation.OperandDictionary.OperandArray);

            if (outputPredicate == null  && knowPredicate == null)
				return;

            JstPredicateReference predRef = knowPredicate != null ?
                    ReferenceDerivation.ForUsePredicate(knowPredicate.ReferencePredicate) :
                    ReferenceDerivation.Target;

            JstSymbolDictionary dict = knowPredicate != null ?
                knowPredicate.SymbolDictionary :
                outputPredicate.SymbolDictionary;

            SymbolDictionary.ImportDictionary(predRef.ReverseTranslateDictionary(dict));

            SetConstantKeys();
		}

        private void SetConstantKeys() 
		{
			var lst = new List<string>();
            foreach (JstOperandInstanciation operand in SymbolDictionary.OperandArray)
            {
                if (operand.OperandValue != "")
                    lst.Add(operand.RefKey);
            }
            ConstantKeys = lst.ToArray<String>();
        }

        public JstValidationResult Validate(Boolean staged)
        {
            JstValidationResult res = new JstValidationResult();
            JstDerivation der = ReferenceDerivation;
            if (der == null)
            {
                res.AddMessage(new JstValidationMessage("Derivation not found"));
                return res;
            }

            Scenario.ValidateSymbolDictionary(SymbolDictionary, res, staged);
            if (!staged || res.IsOK)
                res.AppendMessages(der.Requirement.ValidateWithInput(SymbolDictionary, Scenario));

            return res;
        }

        public String debugString 
		{
            get
            {
                JstDerivation der = ReferenceDerivation;
                if (der == null)
				    return RefKey;

                return RefKey + "TODO";// JstModel.theInstance.translateGraphicSymbols(symbolDictionary.translateString(ref.symbolicInput));
            }
		}

        public void FillOutputPredicate(JstPredicateInstantiation predInst)
        {
            predInst.SetReferencePredicateKey(ReferenceDerivation.Target.RefKey);
            var transDict = ReferenceDerivation.Target.TranslateDictionary(SymbolDictionary);
            predInst.SymbolDictionary.ImportDictionary(transDict);
        }
    }
}
