﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Models
{
    public class Predicate
    {
        public String Key 
        {
            get;
            set;
        }
        public String Name
        {
            get;
            set;
        }
        public String SymbolicDescription
        {
            get;
            set;
        }
        public String SymbolicExpression
        {
            get;
            set;
        }
        public String SymbolicErrorDescription
        {
            get;
            set;
        }
        public Boolean IsSymetric
        {
            get;
            set;
        }
        public Boolean RequiresJustification
        {
            get;
            set;
        }
        public int IconID
        {
            get;
            set;
        }
        public String GlossaryID
        {
            get;
            set;
        }
    }
}
