﻿using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Suggestion
{
    public class JstSuggestionEngine
    {
        private JstScenario Scenario;
        private JstScenario ChosenSolution;
        private Boolean[] IsRedundantArray;



        public JstSuggestionEngine(JstScenario scenario)
        {
            Scenario = scenario;
        }

        public String ChooseSolution(Boolean verbal)
		{
            String debugString = "";
            /*TODO			var bestResult : JstMatchResult = null;
                        var cnt : int = JstModel.theInstance.currentSolutionArray.length;
                        for (var i : int = 0; i<cnt; i++)
                        {
                            if (_verbal)
                                validationResult += validateSolution(JstModel.theInstance.currentSolutionArray[i]);
                    var matchResult : JstMatchResult = generateMatchResult(i, _verbal);
                    debugString += matchResult.mDebugString + "\n";
                            if (matchResult.betterMatchThan(bestResult))
                            {
                                mChosenSolution = i;
                                bestResult = matchResult;
                            }
            }*/
            /*temp:*/
            ChosenSolution = Scenario.SolutionList.Count > 0 ? Scenario.SolutionList[0] : null;

            return debugString;
		}

        private void IdentifyRedundant()
        { 
			IsRedundantArray = Enumerable.Repeat(true, ChosenSolution.KnowList.Count).ToArray();
			
			var goal = Goal(Scenario);
            var solGoal = MatchPredicateInKnow(ChosenSolution, goal);
			if (solGoal == null)
				return;

            MarkNonRedundant(solGoal, true);
        }

        private void MarkNonRedundant(IJstScenarioLine line, Boolean isInTIK)
		{
			if (MatchPredicateInKnow(Scenario, line) != null || MatchPredicateInTrivial(Scenario, line) != null)
				return;
				
			if (isInTIK)
				IsRedundantArray[line.OwnIndex] = false;

			foreach (var k in line.KnowFromIndices)
			{
				var sourceLine = ChosenSolution.KnowList[k];
                MarkNonRedundant(sourceLine, true);
            }
		}

        private Boolean IsRedundant(IJstScenarioLine line)
		{
			return IsRedundantArray[line.OwnIndex];
		}



    public List<JstSuggestion> Suggest(JstDerivationInstantiation der, JstPredicateInstantiation pred)
        {
            var ret = new List<JstSuggestion>();
            ChooseSolution(false);
            if (ChosenSolution == null)
                return ret;

            IdentifyRedundant();

            if (Goal(Scenario) == null)
            {
                AddSingleSuggestion(
                    new JstSuggestion(1, JstSuggestion.PED_SUPPORT_SOLVED_PROBLEM, "", null, null, null, null), 
                        ret);
				return ret;
			}
			
			if (der != null)
			{

                AddSuggestions(suggest_PED_SUPPORT_USE_TIK_WITHIN_TEMPLATE(der, pred), ret);
				if (ret.Count > 0)
					return ret;
			}


            AddSuggestions(suggest_PED_SUPPORT_JUSTIFY_GOAL_mix(), ret);
            AddSuggestions(suggest_PED_SUPPORT_JUSTIFY_CLAIM_mix(), ret);
            AddSuggestions(suggest_PED_SUPPORT_USE_mix(), ret);
            AddSuggestions(suggest_PED_SUPPORT_USE_THEOREM(), ret);
            AddSuggestions(suggest_PED_SUPPORT_JUSTIFY_SUB_GOAL(), ret);
            AddSuggestions(suggest_PED_SUPPORT_FIND_MEASURE(), ret);
            AddSuggestions(suggestAlgebra(), ret);

            return ret.OrderBy(suggestion => suggestion.Priority).ToList();
		}

        private void AddSuggestions(IEnumerable<JstSuggestion> source, List<JstSuggestion> result) 
		{
            if (source == null)
                return;

			foreach (var sugg in source)
                AddSingleSuggestion(sugg, result);
        }

        private void AddSingleSuggestion(JstSuggestion suggestion, List<JstSuggestion> result)
		{
            GenerateHighlightedLines(suggestion);
            suggestion.AddNewClaim = (suggestion.EventKey == JstSuggestion.PED_SUPPORT_JUSTIFY_SUB_GOAL);
			suggestion.TheoreomTemplate = (suggestion.EventKey == JstSuggestion.PED_SUPPORT_USE_TIK_WITHIN_TEMPLATE);
            suggestion.TikKeyList = suggestion.TikIndexList.Select(ind => Scenario.KnowList[ind].ID).ToList();
            suggestion.JustifyKeyList = suggestion.JustifyIndexList.Select(ind => Scenario.JustifyPredicateList[ind].ID).ToList();
            result.Add(suggestion);
		}

		private void GenerateHighlightedLines(JstSuggestion suggestion)
		{
			if (suggestion.LineList != null)
				return;
            suggestion.LineList = new List<IJstScenarioLine>();
			foreach (var t in suggestion.TikIndexList)
                suggestion.LineList.Add(Scenario.KnowList[t]);
            foreach (var j in suggestion.JustifyIndexList)
                suggestion.LineList.Add(Scenario.JustifyPredicateList[j]);
		}

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Utility
        //
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


        private JstPredicateInstantiation Goal(JstScenario scen)
        {
            return (scen.JustifyPredicateList.Count > 0) ? scen.JustifyPredicateList[0] as JstPredicateInstantiation : null;
        }

        private IJstScenarioLine MatchPredicateInJustify(JstScenario scen, IJstScenarioLine line)
        {
            return MatchPredicateIn(scen.JustifyPredicateList, line);
        }

        private IJstScenarioLine MatchPredicateInKnow(JstScenario scen, IJstScenarioLine line)
        {
            return MatchPredicateIn(scen.KnowList, line);
        }
        private IJstScenarioLine MatchPredicateInTrivial(JstScenario scen, IJstScenarioLine line)
        {
            return MatchPredicateIn(scen.TrivialList, line);
        }

        private IJstScenarioLine MatchPredicateIn(List<IJstScenarioLine> lineList, IJstScenarioLine line)
		{
			return lineList.Find(aLine => aLine.SameAs(line));
		}

        private List<IJstScenarioLine> SourcePredicates(JstScenario scen, IJstScenarioLine line, Boolean skipNonPredicates = false, Boolean skipTrivials = false) 
		{
            var ret = new List<IJstScenarioLine>();

			foreach (var k in line.KnowFromIndices)
			{
				var sourceLine = scen.KnowList[k];
                if (skipNonPredicates && (sourceLine as JstPredicateInstantiation == null))
                {
                    List<IJstScenarioLine> addList = SourcePredicates(scen, sourceLine, true);
                    ret.AddRange(addList);
                }
                else
                    ret.Add(sourceLine);	
			}
			if (skipTrivials)
				return ret;
			foreach (var t in line.TrivialFromIndices)
			{
				var sourceLine = scen.TrivialList[t];
				if (MatchPredicateIn(ret, sourceLine) == null)
					ret.Add(sourceLine);	
			}
            return ret;
		}

        private Boolean MatchAllSources(JstScenario scen, List<IJstScenarioLine> sourcePredicates, Boolean algebraThereforeUseOnlyTIK = false) 
		{
			foreach (var line in sourcePredicates)
			{
				if (MatchPredicateInKnow(scen, line) == null)
				{
					if (algebraThereforeUseOnlyTIK || MatchPredicateInTrivial(scen, line) == null)
					{
						return false;
					}
				}
            }
			return true;
		}

        Boolean KnownIsUsed(JstScenario scen, int ind) 
		{
			var kCnt =scen.KnowList.Count;
			for (int k = 0; k<kCnt; k++)
			{
				 var tik = scen.KnowList[k] as JstPredicateInstantiation;
				 if (tik != null && tik.KnowFromIndices.Contains(ind))
					 return true;
			}

			return false;
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Concrete Suggestions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

private List<JstSuggestion> suggest_PED_SUPPORT_USE_TIK_WITHIN_TEMPLATE(JstDerivationInstantiation der, JstPredicateInstantiation pred)
        {
            return null;
        }
        private IEnumerable<JstSuggestion> suggest_PED_SUPPORT_JUSTIFY_GOAL_mix()
        {
            //If there's was no attempt to click the goal (predicate) to see which theorems can justify it	
            // Goal predicate in CIJ
            // 1
            var goal = Goal(Scenario);
            if (goal.FromDerivation != null && goal.FromDerivation.RefKey != "")
                return null;
            var solutionGoal = MatchPredicateInKnow(ChosenSolution, goal);
            if (solutionGoal == null) // INDICATES PROBLEM !!!
                return null;
            if (IsRedundant(solutionGoal))
                return null;
            if (!(solutionGoal.FromDerivation != null && solutionGoal.FromDerivation.RefKey != ""))
                return null; // INDICATES PROVED BY ALGEEBRA
            var sourcePredicates = SourcePredicates(ChosenSolution, solutionGoal);
            Boolean matchAll = MatchAllSources(Scenario, sourcePredicates);
            if (!matchAll || solutionGoal.FromDerivation.RefKey == "")
                return new JstSuggestion[] { new JstSuggestion(3, JstSuggestion.PED_SUPPORT_JUSTIFY_GOAL_PLANNING, "", null, null, new int[] { 0 }, null)};

            return new JstSuggestion[]
                { new JstSuggestion(1, JstSuggestion.PED_SUPPORT_JUSTIFY_GOAL, "", null, null, new int[] { 0 }, null) ,
                  new JstSuggestion(4, JstSuggestion.PED_SUPPORT_JUSTIFY_GOAL_WITH_THEOREM,
                  solutionGoal.FromDerivation.RefKey, null, null, new int[] { 0 }, null) };
        }

        private List<JstSuggestion> suggest_PED_SUPPORT_JUSTIFY_CLAIM_mix()
        {
            // If one of the CIJ predicates (not the goal) are used in the solution and it has not yet been justified
            // 2
            var ret = new List<JstSuggestion>();
            var jCnt = Scenario.JustifyPredicateList.Count;
            for (var j = 1; j < jCnt; j++)
            {
                var ntj = Scenario.JustifyPredicateList[j];
                var ntjDerKey = ntj.FromDerivation != null ? ntj.FromDerivation.RefKey : "";
                var solutionNtj = MatchPredicateInKnow(ChosenSolution, ntj);
                if (solutionNtj == null)
                    continue;
                if (IsRedundant(solutionNtj))
                    continue;
                var solDerKey = solutionNtj.FromDerivation != null ? solutionNtj.FromDerivation.RefKey : "";
                if (solDerKey == "")
                    continue;
                var sourcePredicates = SourcePredicates(ChosenSolution, solutionNtj);
                var ready = MatchAllSources(Scenario, sourcePredicates);
                ret.Add(new JstSuggestion(3, ready ? JstSuggestion.PED_SUPPORT_JUSTIFY_CLAIM : JstSuggestion.PED_SUPPORT_JUSTIFY_CLAIM_PLANNING, 
                    "", null, null, new int[] { j }, null));
                if (ready && (ntjDerKey != solDerKey))
                    ret.Add(new JstSuggestion(4, JstSuggestion.PED_SUPPORT_JUSTIFY_CLAIM_WITH_THEOREM, 
                        solDerKey, null, null, new int[] { j }, null));
            }
            return ret;
        }

        private List<JstSuggestion> suggest_PED_SUPPORT_USE_mix()
        {
            var ret = new List<JstSuggestion>();

            var mayBeUsedIndices = new List<int>();
            var kCnt = ChosenSolution.KnowList.Count;
            int k;
            IJstScenarioLine sceTik;
            for (k = 0; k < kCnt; k++)
            {
                var solTik  = ChosenSolution.KnowList[k] as JstPredicateInstantiation;
                if (solTik == null || solTik.FromDerivation == null || solTik.FromDerivation.RefKey == "")
                    continue;
                if (IsRedundant(solTik))
                    continue;
                sceTik = MatchPredicateInKnow(Scenario, solTik);
                if (sceTik != null)
                    continue;
                var sourcePredArray  = SourcePredicates(ChosenSolution, solTik);
                var allThere = MatchAllSources(Scenario, sourcePredArray);
                if (!allThere)
                    continue;
                var sourceIndices = new List<int>();
                var sCnt = sourcePredArray.Count;
                for (int s = 0; s < sCnt; s++)
				{
                    var solSource  = sourcePredArray[s];
                    var sceSource = MatchPredicateInKnow(Scenario, solSource);
                    if (sceSource as JstPredicateInstantiation != null)
                    {
                        var sceInd  = sceSource.OwnIndex;
                        if (sceSource.Status == JstPredicateInstantiation.STATUS_GIVEN || 
                            sceSource.Status == JstPredicateInstantiation.STATUS_GIVEN_AND_USED)
                            ret.Add(new JstSuggestion(4, JstSuggestion.PED_SUPPORT_USE_THEOREM_WITH_SINGLE_GIVEN, 
                                solTik.FromDerivation.RefKey, null, new int[] { sceInd }, null, null));
                        else
                            ret.Add(new JstSuggestion(4, JstSuggestion.PED_SUPPORT_USE_THEOREM_WITH_SINGLE_TIK, 
                                solTik.FromDerivation.RefKey, null, new int[] { sceInd }, null, null));

                        sourceIndices.Add(sceInd);
                        if (!mayBeUsedIndices.Contains(sceInd))
                            mayBeUsedIndices.Add(sceInd);
                    }
                    else if (MatchPredicateInTrivial(Scenario, solSource) == null)
                        allThere = false;

                }
                if (sourceIndices.Count > 1)
                {
                    // PED_SUPPORT_USE_MULTI_TIK
                    //If there's a combination of predicates in TIK that in the solution lead together to another predicate	
                    //A collection of TIK predicates (not necessary close to one another). Could be a mixture of givens and non-givens
                    //2
                    ret.Add(new JstSuggestion(2, JstSuggestion.PED_SUPPORT_USE_MULTI_TIK, 
                        "", null, sourceIndices, null, null));
                    ret.Add(new JstSuggestion(5, JstSuggestion.PED_SUPPORT_USE_THEOREM_WITH_MULTI_TIK, 
                        solTik.FromDerivation.RefKey, null, sourceIndices, null, null));
                }
            }

            suggestGivenRelated(mayBeUsedIndices, ret);

            return ret;
        }

        private void suggestGivenRelated(List<int> mayBeUsedIndices, List<JstSuggestion> ret)
        {
            var givenNotUsed = false;

            var mCnt = mayBeUsedIndices.Count;
            for (int m = 0; m < mCnt; m++)
            {
                var ind = mayBeUsedIndices[m];
                var mayBeUsedTik = Scenario.KnowList[ind] as JstPredicateInstantiation;
                if (mayBeUsedTik.Status == JstPredicateInstantiation.STATUS_GIVEN)
                    givenNotUsed = true;
                else if (mayBeUsedTik.Status == JstPredicateInstantiation.STATUS_GIVEN_AND_USED)
                {
                    // PED_SUPPORT_USE_GIVEN_AGAIN 
                    //If a given was used but can be used again and is used again in the solution	
                    //A single given predicate
                    //2
                    ret.Add(new JstSuggestion(2, JstSuggestion.PED_SUPPORT_USE_GIVEN_AGAIN, "", null, new int[] { ind }, null, null));
                }
                else if (KnownIsUsed(Scenario, ind))
                {

                    // PED_SUPPORT_USE_SINGLE_TIK_AGAIN
                    //If a predicate in TIK was used but can be used again and is used again in the solution
                    // A single TIK predicate that is not one of the givens
                    //2
                    ret.Add(new JstSuggestion(2, JstSuggestion.PED_SUPPORT_USE_SINGLE_TIK_AGAIN, "", null, new int[] { ind }, null, null));
                }
                else
                {
                    // PED_SUPPORT_USE_SINGLE_TIK
                    //If a predicate in TIK was not used and is used in the solution
                    //A single TIK predicate that is not one of the givens
                    //1
                    ret.Add(new JstSuggestion(1, JstSuggestion.PED_SUPPORT_USE_SINGLE_TIK, "", null, new int[] { ind }, null, null));
                }
            }

            if (givenNotUsed)
            {
                var givenIndices = new List<int>();
                var kCnt = Scenario.KnowList.Count;
                for (int k = 0; k < kCnt; k++)
                {
                    var sceTik = Scenario.KnowList[k] as JstPredicateInstantiation;
                    if (sceTik != null && (sceTik.Status == JstPredicateInstantiation.STATUS_GIVEN || sceTik.Status == JstPredicateInstantiation.STATUS_GIVEN_AND_USED))
                        givenIndices.Add(k);
                }
                //PED_SUPPORT_USE_GIVEN
                //If one of the givens was not used by the student but was used in the solution	
                //All the block of given predicates in TIK.
                // 1
                ret.Add(new JstSuggestion(1, JstSuggestion.PED_SUPPORT_USE_GIVEN, "", null, givenIndices, null, null));
            }

        }

        private List<JstSuggestion> suggest_PED_SUPPORT_USE_THEOREM()
		{
			var kCnt  = ChosenSolution.KnowList.Count;
            var ret = new List<JstSuggestion>();

            var allreadyReported = new List<String>(); ;

			for (int k = 0; k < kCnt; k++)
			{
				var solTik = ChosenSolution.KnowList[k] as JstPredicateInstantiation;
				if (solTik == null)
					continue;
				if (IsRedundant(solTik))
					continue;
				var scenTik = MatchPredicateInKnow(Scenario, solTik);
				if (scenTik != null)
					continue;
				var derKey = solTik.FromDerivation != null ? solTik.FromDerivation.RefKey : "";
				if (derKey == "" || allreadyReported.Contains(derKey))
					continue;

				var sourcePredArray  = SourcePredicates(ChosenSolution, solTik);
				var allThere = MatchAllSources(Scenario, sourcePredArray);
				
				if (allThere)
				{
					ret.Add(new JstSuggestion(3, JstSuggestion.PED_SUPPORT_USE_THEOREM, derKey, null, null, null, null));
					allreadyReported.Add(derKey);
				}
			}
			return ret;
		}

        private List<JstSuggestion> suggest_PED_SUPPORT_JUSTIFY_SUB_GOAL()
        {
            // If the student has enough predicates to justify a claim that is in the solution but that they did not make yet
            // The "click to add a new claim"	
            //One claim that can help you is <sub-goal>. Add it and see if you can justify it
            //3
            var ret = new List<JstSuggestion>();
            var kCnt = ChosenSolution.KnowList.Count - 1; // should not take target
            for (int k = 0; k < kCnt; k++)
			{
                var solTik = ChosenSolution.KnowList[k] as JstPredicateInstantiation;
                if (solTik == null || solTik.Status != JstPredicateInstantiation.STATUS_NORMAL)
                    continue;
                if (solTik.FromDerivation != null || solTik.FromDerivation.RefKey == "")
                    continue;
                if (IsRedundant(solTik))
                    continue;
                if (MatchPredicateInKnow(Scenario, solTik) != null || MatchPredicateInJustify(Scenario, solTik) != null)
                    continue;
                var sourcePredicates = SourcePredicates(ChosenSolution, solTik);
                var matchAll = MatchAllSources(Scenario, sourcePredicates);
                if (matchAll)
                    ret.Add(new JstSuggestion(3, JstSuggestion.PED_SUPPORT_JUSTIFY_SUB_GOAL, 
                        "", new List<IJstScenarioLine> { solTik }, null, null, null));
            }

            return ret;
        }
        private List<JstSuggestion> suggest_PED_SUPPORT_FIND_MEASURE()
        {
            var ret = new List<JstSuggestion>();
            var kCnt = ChosenSolution.KnowList.Count;
            for (int k = 0; k < kCnt; k++)
            {
                var solTik = ChosenSolution.KnowList[k] as JstPredicateInstantiation;
                if (solTik == null)
                    continue;
                if (IsRedundant(solTik))
                    continue;
                String element = Scenario.MeasuredElement(solTik);
                if (element == "")
                    continue;
                if (MatchPredicateInKnow(Scenario, solTik) != null|| MatchPredicateInJustify(Scenario, solTik) != null)
                    continue;
                var sourcePredicates = SourcePredicates(ChosenSolution, solTik);
                var matchAll = MatchAllSources(Scenario, sourcePredicates);
                if (!matchAll)
                    continue;

                ret.Add(new JstSuggestion(3, JstSuggestion.PED_SUPPORT_FIND_MEASURE_SPECIFIC, "", null, null, null, element.Split('-')));
            }
            if (ret.Count > 0)
                ret.Add(new JstSuggestion(2, JstSuggestion.PED_SUPPORT_FIND_MEASURE_GENERAL, "", null, null, null, null));
            return ret;
        }
        private List<JstSuggestion> suggestAlgebra(){ return null; }

    }
}
