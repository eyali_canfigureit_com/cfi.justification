﻿using cfi.justification.lib.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib.Suggestion
{
    public class JstSuggestion
    {
        public String EventKey { get; private set; }
        public String DerivationKey { get; private set; }
        public List<int> TikIndexList { get; private set; }
        public List<int> JustifyIndexList { get; private set; }
        public int Priority { get; private set; }

        public List<String> ElementList { get; private set; }

        public Boolean AddNewClaim { get; set; }
        public Boolean TheoreomTemplate { get; set; }
        public List<IJstScenarioLine> LineList { get; set; }

        //public function get derivation() : JstDerivation {return mDerivationKey == "" ? null : JstModel.theInstance.derivationByKey(mDerivationKey);}
        public String Text { get { return TempTextImp(); } }
        public List<String> TikKeyList { get; set; }
        public List<String> JustifyKeyList { get; set; }

        public const String PED_SUPPORT_JUSTIFY_GOAL = "PED_SUPPORT_JUSTIFY_GOAL";
		public const String PED_SUPPORT_JUSTIFY_GOAL_PLANNING = "PED_SUPPORT_JUSTIFY_GOAL_PLANNING";
		public const String PED_SUPPORT_JUSTIFY_CLAIM = "PED_SUPPORT_JUSTIFY_CLAIM";
		public const String PED_SUPPORT_JUSTIFY_CLAIM_PLANNING = "PED_SUPPORT_JUSTIFY_CLAIM_PLANNING";
		public const String PED_SUPPORT_USE_GIVEN = "PED_SUPPORT_USE_GIVEN";
		public const String PED_SUPPORT_USE_GIVEN_AGAIN  = "PED_SUPPORT_USE_GIVEN_AGAIN"; 
		public const String PED_SUPPORT_USE_SINGLE_TIK = "PED_SUPPORT_USE_SINGLE_TIK";
		public const String PED_SUPPORT_USE_SINGLE_TIK_AGAIN  = "PED_SUPPORT_USE_SINGLE_TIK_AGAIN"; 
		public const String PED_SUPPORT_USE_MULTI_TIK  = "PED_SUPPORT_USE_MULTI_TIK";
		public const String PED_SUPPORT_USE_THEOREM = "PED_SUPPORT_USE_THEOREM";
		public const String PED_SUPPORT_USE_THEOREM_WITH_SINGLE_GIVEN = "PED_SUPPORT_USE_THEOREM_WITH_SINGLE_GIVEN";
		public const String PED_SUPPORT_USE_THEOREM_WITH_SINGLE_TIK = "PED_SUPPORT_USE_THEOREM_WITH_SINGLE_TIK";
		public const String PED_SUPPORT_USE_THEOREM_WITH_MULTI_TIK = "PED_SUPPORT_USE_THEOREM_WITH_MULTI_TIK";
		public const String PED_SUPPORT_JUSTIFY_GOAL_WITH_THEOREM = "PED_SUPPORT_JUSTIFY_GOAL_WITH_THEOREM";
		public const String PED_SUPPORT_JUSTIFY_CLAIM_WITH_THEOREM = "PED_SUPPORT_JUSTIFY_CLAIM_WITH_THEOREM";
		public const String PED_SUPPORT_JUSTIFY_SUB_GOAL = "PED_SUPPORT_JUSTIFY_SUB_GOAL";
		public const String PED_SUPPORT_FIND_MEASURE_GENERAL = "PED_SUPPORT_FIND_MEASURE_GENERAL"; 
		public const String PED_SUPPORT_FIND_MEASURE_SPECIFIC = "PED_SUPPORT_FIND_MEASURE_SPECIFIC";
		public const String PED_SUPPORT_USE_TIK_WITHIN_TEMPLATE = "PED_SUPPORT_USE_TIK_WITHIN_TEMPLATE";
		
		public const String PED_SUPPORT_USE_ALGEBRA_GENERAL = "PED_SUPPORT_USE_ALGEBRA_GENERAL"; 
		public const String PED_SUPPORT_USE_ALGEBRA_WITH_SINGLE_TIK = "PED_SUPPORT_USE_ALGEBRA_WITH_SINGLE_TIK";
		public const String PED_SUPPORT_USE_ALGEBRA_WITH_MULTI_TIK = "PED_SUPPORT_USE_ALGEBRA_WITH_MULTI_TIK";
		public const String PED_SUPPORT_USE_ALGEBRA_SUBSTITUTE_WITH_MULTI_TIK = "PED_SUPPORT_USE_ALGEBRA_SUBSTITUTE_WITH_MULTI_TIK";
		public const String PED_SUPPORT_USE_ALGEBRA_TO_JUSTIFY_CLAIM = "PED_SUPPORT_USE_ALGEBRA_TO_JUSTIFY_CLAIM";
		public const String PED_SUPPORT_USE_ALGEBRA_TO_JUSTIFY_GOAL = "PED_SUPPORT_USE_ALGEBRA_TO_JUSTIFY_GOAL";
		public const String PED_SUPPORT_USE_ALGEBRA_TO_NEW_EXPRESSION = "PED_SUPPORT_USE_ALGEBRA_TO_NEW_EXPRESSION";

		public const String PED_SUPPORT_SOLVED_PROBLEM = "PED_SUPPORT_SOLVED_PROBLEM";
		
		public string DebugString() 
		{
			var ret =  "(" + Priority + ") " + EventKey + "\n   der: " + (DerivationKey != "" ? DerivationKey : "NONE") + "\n   k:";
			foreach( var i in TikIndexList)
                ret  += " " + i;
   			ret += "; j:";
   			foreach(var j in JustifyIndexList)
                ret  += " " + j;
			ret += "; e:";
			foreach(var e in ElementList)
                ret  += " " + e;
			foreach(var l  in LineList)
                ret  += "\n   " + l.DebugString;
			ret += "\n AddNewClaim: " + AddNewClaim + " TheoreomTemplate: "+ TheoreomTemplate;
			return ret;
		}
		
		public JstSuggestion(int pri, String eventKey, String derivationKey,
            IEnumerable<IJstScenarioLine> lineList,
            IEnumerable<int> tikList, IEnumerable<int> justList,
            IEnumerable<String> elementList)
        {
            Priority = pri;
            EventKey = eventKey;
            DerivationKey = derivationKey;
            LineList = lineList == null ? null : lineList.ToList();
            TikIndexList = tikList == null? new List<int>() : tikList.ToList();
            JustifyIndexList = justList == null ? new List<int>() : justList.ToList();
            ElementList = elementList == null ? new List<String>() : elementList.ToList();
            AddNewClaim = false;
            TheoreomTemplate = false;
        }

        private String TempTextImp() 
		{
            string[] arr = new string[] {
                "PED_SUPPORT_JUSTIFY_GOAL",
                "Click the goal <JSTLine> and see if you can justify it",
                "PED_SUPPORT_JUSTIFY_GOAL_PLANNING",
                "Click the goal to see theorems that might help you justify it <JSTLine> Think what you would need to know to use these theorems",
                "PED_SUPPORT_JUSTIFY_CLAIM",
                "Click the claim <JSTLine> and see if you can justify it",
                "PED_SUPPORT_JUSTIFY_CLAIM_PLANNING",
                "Click this claim to see theorems that might help you justify it <JSTLine> Think about what you would need to know to use these theorems",
                "PED_SUPPORT_USE_GIVEN",
                "See if you used all your givens <JSTLine>",
                "PED_SUPPORT_USE_GIVEN_AGAIN",
                "Try to use this given again <JSTLine>",
                "PED_SUPPORT_USE_SINGLE_TIK",
                "See if you can use the following information <JSTLine>",
                "PED_SUPPORT_USE_SINGLE_TIK_AGAIN",
                "See if you can use the following information again <JSTLine>",
                "PED_SUPPORT_USE_MULTI_TIK",
                "See if you can use the following information together <JSTLine>",
                "PED_SUPPORT_USE_THEOREM",
                "See if you can use the following theorem <Der>",
                "PED_SUPPORT_USE_THEOREM_WITH_SINGLE_GIVEN",
                "See if you can use the following theorem <Der> together with this given <JSTLine>",
                "PED_SUPPORT_USE_THEOREM_WITH_SINGLE_TIK",
                "See if you can use the following theorem <Der> together with this information <JSTLine>",
                "PED_SUPPORT_USE_THEOREM_WITH_MULTI_TIK",
                "See if you can use the following theorem <Der> together with this information <JSTLine>",
                "PED_SUPPORT_JUSTIFY_GOAL_WITH_THEOREM",
                "See if you can use the following theorem <Der> to justify the goal <JSTLine>",
                "PED_SUPPORT_JUSTIFY_CLAIM_WITH_THEOREM",
                "See if you can use the following theorem <Der> to justify this claim <JSTLine>",
                "PED_SUPPORT_JUSTIFY_SUB_GOAL",
                "It might help you to know that <JSTLine> Add it as a new claim and see if you can justify it",
                "PED_SUPPORT_FIND_MEASURE_GENERAL",
                "Try to find some measures in the diagram",
                "PED_SUPPORT_FIND_MEASURE_SPECIFIC",
                "Try to find the measure of <element>",
                "PED_SUPPORT_USE_TIK_WITHIN_TEMPLATE",
                "Use the fact you already know <JSTLine> to fill in the theorem template",
                "TEMP_ALGEBRA_SHOW",
                "Use Algebra to show that <JSTLine>",
                "TEMP_ALGEBRA_USE",
                "Use the fact you already know <JSTLine> with Algebra",
                "PED_SUPPORT_USE_ALGEBRA_GENERAL",
                "Try to use ALGEBRA to make progress",
                "PED_SUPPORT_USE_ALGEBRA_WITH_SINGLE_TIK",
                "Use ALGEBRA to develop the following <JSTLine>",
                "PED_SUPPORT_USE_ALGEBRA_WITH_MULTI_TIK",
                "Use ALGEBRA to work with these together <JSTLine>",
                "PED_SUPPORT_USE_ALGEBRA_SUBSTITUTE_WITH_MULTI_TIK",
                "Try to use the SUBSTITUTION operation and this information <JSTLine>",
                "PED_SUPPORT_USE_ALGEBRA_TO_JUSTIFY_CLAIM",
                "Use ALGEBRA to get to this claim <JSTLine>",
                "PED_SUPPORT_USE_ALGEBRA_TO_JUSTIFY_GOAL",
                "Use ALGEBRA to get to the goal <JSTLine>",
                "PED_SUPPORT_USE_ALGEBRA_TO_NEW_EXPRESSION",
                "It might help you to know  that <JSTLine> See if you can get to it using algebra",
                "PED_SUPPORT_SOLVED_PROBLEM",
                "It seems like this problem is solved - Go ahead and try another one"
                };
                int ind = Array.IndexOf(arr, EventKey);
			    if (ind == -1)
				    return "text for '" + EventKey + "' not found...";
			    return arr[ind + 1];
		}
    }
}
