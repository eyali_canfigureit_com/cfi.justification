﻿using cfi.justification.lib.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cfi.justification.lib
{
    public interface IJstPredicateRepository
    {
        JstPredicate[] AllPredicates();
        JstPredicate PredicateByID(string key);
    }
}
