﻿using cfi.geometry.lib.ObjectModel;
using cfi.geometry.lib.Scene;
using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.DomnainMediation
{
    public class GeoDomainMediatorLoader : IJstDomainMediatorLoader
    {
        public static GeoDomainMediatorLoader Loader = new GeoDomainMediatorLoader();
        GeoDomainMediatorLoader() { RegisterMediator("model", this); }

        public override IJstDomainMediator Domain() { return new GeoDomainMediator(); }

    }

    class GeoDomainMediator : IJstDomainMediator
    {
        public const String JST_POINT = "JST_POINT";
        public const String JST_SEGMENT = "JST_SEGMENT";
        public const String JST_ANGLE = "JST_ANGLE";
        public const String JST_TRIANGLE = "JST_TRIANGLE";
        public const String JST_QUAD = "JST_QUAD";
        public const String JST_CIRCLE = "JST_CIRCLE";
        public const String JST_NUMBER = "JST_NUMBER";
        //		public const String[] TYPE_ARRAY = { JST_POINT, JST_SEGMENT, JST_ANGLE, JST_TRIANGLE, JST_QUAD, JST_CIRCLE, JST_NUMBER };
        private String[] TYPE_ARRAY = { JST_POINT, JST_SEGMENT, JST_ANGLE, JST_TRIANGLE, JST_QUAD, JST_CIRCLE, JST_NUMBER };

        private String[] pointLabelArray = { };
        private String[] segmentLabelArray = { };
        private String[] angleLabelArray = { };
        private String[] triangleLabelArray = { };
        private String[] quadLabelArray = { };
        private String[] circleLabelArray = { };
        private GeoScene scene;

        public override void ReadDomainFromXML(XmlNode xml, String id)
        {
            scene = GeoSceneRepository.TheInstance.CreateScene(id, xml.SelectNodes("model")[0]);
            BuildOperandLabelArrays();
        }

        public override String[] TypeArray { get { return new String[0]; } }
        public override String[] KnownOperandsForType(String type)
        {
            switch (type)
            {
                case JST_POINT:
                    return pointLabelArray;
                case JST_SEGMENT:
                    return segmentLabelArray;
                case JST_ANGLE:
                    return angleLabelArray;
                case JST_CIRCLE:
                    return circleLabelArray;
            }
            return new String[0];
        }

        public override void ValidateSymbolDictionary(JstSymbolDictionary symDict, JstValidationResult result, Boolean staged)
        {
            foreach (JstOperandInstanciation operand in symDict.OperandArray)
            {
                if (operand.OperandValue != "" || !staged)
                    ValidateSymbolValue(operand.RefKey, symDict.GetTypeForKey(operand.RefKey), operand.OperandValue, result);
            }

        }

        private void ValidateSymbolValue(String key, String typ, String val, JstValidationResult res)
        {
            JstValidationMessage msg = null;
            if (val == "")
                msg = new JstValidationMessage(JstMessageBroker.MessageByCode(JstMessageBroker.JST_MANDATORY));
            else if (typ == JST_NUMBER)
            {
                double Num;
                bool isNum = double.TryParse(val, out Num);
                if (!isNum)
                    msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_NUMBER, val));
            }
            else if (Array.IndexOf<String>(KnownOperandsForType(typ), val) == -1)
            {
                switch (typ)
                {
                    case JST_SEGMENT:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_SEGMENT, val));
                        break;
                    case JST_POINT:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_POINT, val));
                        break;
                    case JST_CIRCLE:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_CIRCLE, val));
                        break;
                    case JST_ANGLE:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_ANGLE, val));
                        break;
                    case JST_TRIANGLE:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_TRIANGLE, val));
                        break;
                    case JST_QUAD:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_QUAD, val));
                        break;
                    default:
                        msg = new JstValidationMessage(JstMessageBroker.MessageByCodeSingleParameter(JstMessageBroker.JST_NO_TYPE, typ));
                        break;
                }
            }
            if (msg != null)
            {
                msg.Reference = new String[] { key };
                res.AddMessage(msg);
            }
        }

        void BuildOperandLabelArrays()
        {
            pointLabelArray = BuildLabelArrayFrom(from point in scene.Extension.PointList select (point as GeoBase));
            segmentLabelArray = BuildLabelArrayFrom(from section in scene.Extension.SectionList select (section as GeoBase));
            angleLabelArray = BuildLabelArrayFrom(from angle in scene.Extension.AngleList select (angle as GeoBase));
            triangleLabelArray = BuildLabelArrayFrom(from triangle in scene.Extension.TriangleList select (triangle as GeoBase));
            quadLabelArray = BuildLabelArrayFrom(from quad in scene.Extension.QuadlateralList select (quad as GeoBase));
            circleLabelArray = BuildLabelArrayFrom(from circle in scene.Extension.CircleList select (circle as GeoBase));
        }

        private String[] BuildLabelArrayFrom(IEnumerable<GeoBase> list)
        {
            List<string> ret = new List<string>(0);
            foreach (GeoBase geo in list)
                ret.AddRange(geo.Labels);
            return ret.ToArray<String>();
        }

        public override bool MatchOperand(String val1, String val2, String opType)
        {
            if (val1 == val2)
                return true;

            switch (opType)
            {
                case JST_SEGMENT:
                    return Reverse(val1) == val2;
                case JST_POINT:
                    return false;
                case JST_CIRCLE:
                    return false;
                case JST_ANGLE:
                    return false;
                case JST_TRIANGLE:
                    return false;
                case JST_QUAD:
                    return false;
                default:
                   return false;
            }

        }

        private static String Reverse(String s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new String(charArray);
        }

        public override String MeasuredElement(JstPredicateInstantiation tik)
        {
            if (tik == null)
                return "";

            switch (tik.RefKey)
            {
                case "MEASURE_OF_SEGMENT":
                    return tik.SymbolDictionary.TranslateKey("seg") + "-" + JST_SEGMENT;
                case "MEASURE_OF_ANGLE":
                    return tik.SymbolDictionary.TranslateKey("ang") + "-" + JST_ANGLE;
                default:
                    break;
            }
            return "";
        }

    }
}
