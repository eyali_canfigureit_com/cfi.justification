﻿using cfi.justification.lib.Instantiation;
using cfi.justification.lib.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace cfi.justification.lib.DomnainMediation
{

    public abstract class IJstDomainMediatorLoader
    {
        private static Dictionary<string, IJstDomainMediatorLoader> DomainDictionary = new Dictionary<string, IJstDomainMediatorLoader>();
        public static void RegisterMediator(string key, IJstDomainMediatorLoader loader)
        {
            DomainDictionary.Add(key, loader);
        }
        public static IJstDomainMediator MediatorByDomain(string key)
        {
            return DomainDictionary[key].Domain();
        }

        public abstract IJstDomainMediator Domain();
    }

    public abstract class IJstDomainMediator
    {
 
        public abstract void ReadDomainFromXML(XmlNode xml, String id);
        //		function writeDomainToXML(_xml : XML) : void;
        public abstract String[] TypeArray { get; }
        public abstract String[] KnownOperandsForType(String type);
        public abstract void ValidateSymbolDictionary(JstSymbolDictionary symDict, JstValidationResult result, Boolean staged);
        public abstract bool MatchOperand(String val1, String val2, String opType);

        //		function validatePredicateInstanciation(_predInst : JstPredicateInstanciation, _staged : Boolean) : JstValidationResult;
        //		function validateSymbolDictionary(_symDict : JstSymbolDictionary, _res : JstValidationResult, _staged : Boolean = false) : void;
        //		function matchPredicateInstanciation(_knownPred : JstPredicateInstanciation, _candPred : JstPredicateInstanciation) : Boolean
        //       function refereshOperands() : void;
        //		function noramlizeScenario() : void
        //		function nonTrivialOperandsForType(_type : String) : Array;
        //		function get categoryArray() : Array;
        //		function displayPredicateInstanciation(_tik  : JstPredicateInstanciation) : void;
        public abstract String MeasuredElement(JstPredicateInstantiation tik);
//		function scenarioIsReady() : void;

        // UI interaction 
        //		function focusOnField(_dict : JstSymbolDictionary, _sym : String, _position : int) : void;
        //		function focusOnLine(_line : IJstScenarioLine) : void;
        //		function focusOnDerivationInstantiation(_der : JstDerivationInstanciation) : void;
        //		function focusOnSuggestion(_suggestion : JstSuggestion) : void;

        // Event Collection
        //		function AddEventToCollection(_eventName : String, _eventParams : Object) : void;
    }
}
